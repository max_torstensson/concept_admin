<?php
include 'curl.php';
include 'auth.php';
/**
 * API-settings OpenContent
 * @return string
 */
function get_oc_api_base_path()
{
    return get_base_path();
}

function get_oc_api_credentials()
{
    return get_credentials();
}

/**
 * API-settings Newspilot
 * @return string
 */
function get_np_api_base_path()
{
    return "http://172.22.0.84:8080/webservice";
}

function get_np_api_credentials()
{
    // Get credentionals from javascript from current user
    return $_GET['user'] . ":" . $_GET['passw'];
}

/**
 * Get user from querystring
 */
function get_user()
{
    if (isset($_GET['user'])) {
        if ($_GET['user'] == "tagadmin") {
            print "?user=tagadmin";
        }
    }
}

/**
 * Replace text in xml string
 * @param $str
 * @param $needle_start
 * @param $needle_end
 * @param $replacement
 * @return mixed
 */
function replace_between($str, $needle_start, $needle_end, $replacement)
{
    $pos = strpos($str, $needle_start);
    $start = $pos === false ? 0 : $pos + strlen($needle_start);

    $pos = strpos($str, $needle_end, $start);
    $end = $start === false ? strlen($str) : $pos;

    return substr_replace($str, $replacement, $start, $end - $start);
}

/**
 * Get tags from OC with search functions
 * @return mixed
 */
function getTags($add_asterix = true, $type = "tag")
{
    $url = get_oc_api_base_path() . "/search?contenttype=Concept&limit=" . $_GET['limit'] . "&start=" . ($_GET['limit'] * $_GET['start']);
    $query = "";

    $search_value = "";
    if (isset($_GET['search']))
        $search_value = $_GET['search'];

    // Sortering
    if ($_GET['sorting'] == "") {
        $search_value = str_replace(" ", "\\ ", $search_value);
        $query = "(Name:$search_value*^3 OR ConceptDefinitionLong:$search_value*^2 OR ConceptDefinitionShort:$search_value*) ";
    } else {
        if (isset($_GET['search'])) {
            $query = $_GET['search'];
            if ($add_asterix)
                $query .= "*";
        }
    }


    // Status
    if (isset($_GET['ConceptStatus'])) {
        if ($query != "")
            $query .= " AND ";
        $query .= $_GET['ConceptStatus'];
    }    // Type
    if ($_GET['type'] != "all") {
        $query .= " AND ConceptImType:" . $_GET['type'];
    } else if ($type !== "tag")
        $query .= " AND ConceptImType:" . $type;

    // Encode query
    if ($query != "")
        $url .= "&q=" . urlencode($query);

    // Sorting for created and name
    if ($_GET['sorting'] == "Publiceringsdag")
        $url .= "&sort.name=Publiceringsdag";
    else if ($_GET['sorting'] == "updated")
        $url .= "&sort.name=Updated";
    else if ($_GET['sorting'] == "alpha")
        $url .= "&sort.name=Name";

    $data = get_api_data($url, get_oc_api_credentials());
    return json_decode($data);
}

/**
 * Search for tags with defined query
 * @param $query
 * @return mixed
 */
function searchForTags($query)
{
    $url = get_oc_api_base_path() . "/search?contenttype=Concept&sort.name=Name&limit=10&start=0&q=" . urlencode($query);
    $data = get_api_data($url, get_oc_api_credentials());
    return json_decode($data);
}

/**
 * Check if a property exists in an object
 * @param $prop
 * @param $properties
 * @param $icon
 * @param $title
 * @return string
 */
function hasProperty($prop, $properties, $icon, $title)
{
    if (isset($properties->$prop))
        return "<i class=\"fa " . $icon . "\" title=\"" . $title . "\"></i>";
    else
        return "";
}

/**
 * Get one tag from OC
 * @return mixed
 */
function getOneTag($id)
{
    $url = get_oc_api_base_path() . "/objects/" . $id . "/properties";
    $data = get_api_data($url, get_oc_api_credentials());
    return $data;
}

function getOneTagXMl($id)
{
    $url = get_oc_api_base_path() . "/objects/" . $id;
    $data = get_api_data($url, get_oc_api_credentials());
    return $data;
}

/**
 * Get property $key from OC object
 * @param $key
 * @param $properties
 * @return null
 */
function get_value_in_oc_properties($key, $properties)
{
    foreach ($properties as $prop) {
        if ($prop->name == $key)
            return $prop->values;
    }
    return null;
}

/**
 * Convert date from Open Content
 * @param $date
 * @return string
 */
function convert_date_from_oc($date)
{
    date_default_timezone_set("Europe/Stockholm");
    $new_date = new DateTime($date);
    return $new_date->format("Y-m-d H:i");
}

/**
 * Get text between xmltags to be used in userdata from newspilot
 * @param $string
 * @param $tagname
 * @return mixed
 */
function getTextBetweenTags($string, $tagname)
{
    preg_match_all("#<$tagname.*?>([^<]+)</$tagname>#", $string, $matches);
    return $matches;
}

/**
 * Get etag from headerarray
 * @param $headers
 * @return null|string
 */
function get_etag($headers)
{
    if (is_array($headers)) {
        foreach ($headers as $header) {
            if (substr($header, 0, 4) == "ETag")
                return substr(substr($header, 7), 0, -2); // Remove " and line break
        }
    }
    return null;
}

/**
 * Create XML file for tags
 * @param $guid
 * @param $date_versionCreated
 * @param $date_created
 * @param $name
 * @param $type
 * @param $description_long
 * @param $description_short
 * @param $target_dir
 * @param $status
 * @param $links
 * @return string
 */
function create_xml_file($guid, $date_versionCreated, $date_created, $name, $firstname, $lastname, $email, $phone, $streetadress, $postalcode, $country, $type, $description_long, $description_short, $target_dir, $status, $note, $links, $np, $ad)
{

    $xml_filename = $guid . '.xml';
    $xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8" ?><conceptItem xmlns="http://iptc.org/std/nar/2006-10-01/" guid="' . $guid . '" version="1" standard="NewsML-G2" standardversion="2.22" conformance="power"/>');
    $xml->addChild('catalogRef')->addAttribute("href", "http://www.iptc.org/std/catalog/catalog.IPTC-G2-Standards_27.xml");
    $xml->addChild('catalogRef')->addAttribute("href", "http://infomaker.se/spec/catalog/catalog.infomaker.g2.1_0.xml");
    // itemMeta tag
    $item_meta = $xml->addChild('itemMeta');
    $item_meta->addChild('itemClass')->addAttribute("qcode", "cinat:concept");
    $item_meta->addChild('provider')->addAttribute("literal", "Mittmedia Concept Admin");
    $item_meta->addChild('versionCreated', $date_versionCreated);
    $item_meta->addChild('firstCreated', $date_created);
    $item_meta->addChild('pubStatus')->addAttribute("qcode", "stat:" . $status);
    //$item_meta->addChild('edNote', $note);
    $itemMetaExtProperty = $item_meta->addChild('itemMetaExtProperty');
    $itemMetaExtProperty->addAttribute("type", "imext:type");
    $itemMetaExtProperty->addAttribute("value", "x-im/" . $type);
    if ($type == "category") {
        $itemMetaExtPropertyCategory = $item_meta->addChild('itemMetaExtProperty');
        $itemMetaExtPropertyCategory->addAttribute("type", "mmext:category_path");
        $itemMetaExtPropertyCategory->addAttribute("value", $name);
    }
    if ($type == "author" || $type == "person") {
        $itemMetaExtPropertyFirstname = $item_meta->addChild('itemMetaExtProperty');
        $itemMetaExtPropertyFirstname->addAttribute("type", "imext:firstName");
        $itemMetaExtPropertyFirstname->addAttribute("value", $firstname);

        $itemMetaExtPropertyLastname = $item_meta->addChild('itemMetaExtProperty');
        $itemMetaExtPropertyLastname->addAttribute("type", "imext:lastName");
        $itemMetaExtPropertyLastname->addAttribute("value", $lastname);

    }
    $links_item = $item_meta->addChild('links');
    $links_item->addAttribute("xmlns", "http://www.infomaker.se/newsml/1.0");
    $links_item->addChild('link')->addAttribute("rel", "creator"); //->addAttribute("type", "x-im/user")->addAttribute("uuid", "");
    if ($links != "") {
        foreach ($links as $item) {
            $one_link = $links_item->addChild('link');
            $one_link->addAttribute("rel", "irel:seeAlso");

            if ($item['type'] == "x-im/social+facebook")
                $one_link->addAttribute("type", "x-im/social+facebook");
            elseif ($item['type'] == "x-im/social+twitter")
                $one_link->addAttribute("type", "x-im/social+twitter");
            elseif ($item['type'] == "x-im/social+gravatar")
                $one_link->addAttribute("type", "x-im/social+gravatar");
            elseif ($item['type'] == "x-np/user")
                $one_link->addAttribute("type", "x-np/user");
            elseif ($item['type'] == "x-ad/sam_account_name")
                $one_link->addAttribute("type", "x-ad/sam_account_name");
            else
                $one_link->addAttribute("type", "text/html");
            $one_link->addAttribute("url", $item['link']);
        }
    }

    // Concept tag
    $concept = $xml->addChild('concept');
    $conceptId = $concept->addChild('conceptId');
    $conceptId->addAttribute("created", $date_created);
    $conceptId->addAttribute("qcode", "imcpt:" . $guid);
    $conceptId->addAttribute("uri", "x-im://" . $guid);
    $concept->addChild('type')->addAttribute("qcode", "cpnat:" . getTypeValue($type));
    $concept->addChild('name', $name);
    $concept->addChild('definition', $description_long)->addAttribute("role", "drol:long");
    $concept->addChild('definition', $description_short)->addAttribute("role", "drol:short");
    if ($type == "author") {
        $metadata = $concept->addChild('metadata');
        $metadata->addAttribute('xmlns', 'http://www.infomaker.se/newsml/1.0');
        $object = $metadata->addChild('object');
        $key = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 11);
        $object->addAttribute('id', $key);
        $object->addAttribute('type', 'x-im/contact-info');
        $data = $object->addChild('data');
        $data->addChild('email', $email);
        $data->addChild('phone', $phone);
        $data->addChild('streetAddress', $streetadress);
        $data->addChild('postalCode', $postalcode);
        $data->addChild('country', $country);
    }
    // Save XML-file
    $xml->asXML($target_dir . $xml_filename);
    return $xml_filename;
}

function Update_status($guid, $target_dir, $status, $ex_xml_file, $date_versionCreated)
{

    $xml_filename = $guid . '.xml';
    $xml = simplexml_load_string($ex_xml_file);
    //versioncreated
    $xml->itemMeta[0]->versionCreated = $date_versionCreated;
    // status
    $xml->itemMeta[0]->pubStatus['qcode'] = "stat:" . $status;
    //type

    $xml->asXML($target_dir . $xml_filename);
    return $xml_filename;
}

Function update_xml_file($guid, $date_versionCreated, $name, $firstname, $lastname, $email, $phone, $streetadress, $postalcode, $country, $type, $description_long, $description_short, $target_dir, $status, $links, $removedlinks, $ex_xml_file, $link_array_oc)
{
    $xml_filename = $guid . '.xml';
    $xml = simplexml_load_string($ex_xml_file);
    //versioncreated
    $xml->itemMeta[0]->versionCreated = $date_versionCreated;
    // status
    $xml->itemMeta[0]->pubStatus['qcode'] = "stat:" . $status;
    //Type, Firstname and lastname
    foreach ($xml->itemMeta[0]->itemMetaExtProperty as $prop) {
        foreach ($prop->attributes() as $attribute) {
            if ($attribute == "imext:type") {
                $prop->attributes()->value = "x-im/" . $type;
            }
            if ($attribute == "imext:firstName") {
                if ($type == "author" || $type == "person") {
                    $prop->attributes()->value = $firstname;
                }
            }
            if ($attribute == "imext:lastName") {
                if ($type == "author" || $type == "person") {
                    $prop->attributes()->value = $lastname;
                }
            }
        }
    }
    $links_item = $xml->itemMeta[0]->links;
    //remove links from xml
    $i = -1;
    foreach ($links_item->link as $link) {
        $i++;
        if (in_array($link->attributes()->url, $removedlinks)) {
            unset($links_item->link[$i]);
        }
    }
    //remove duplicates
    $links_array = $links_item;

    foreach ($links_array as $key => $link) {
        if (in_array($link, $link_array_oc)) {
            unset($links_array[$key]);
        }
        if (in_array($link, $link_array_oc)) {
            unset($links_array[$key]);
        }
    }
    $links_array = array_filter($links_array);


    //add links
    if (count($links) > 0) {
        foreach ($links as $item) {
            $one_link = $links_item->addChild('link');
            $one_link->addAttribute("rel", "irel:seeAlso");

            if ($item['type'] == "x-im/social+facebook")
                $one_link->addAttribute("type", "x-im/social+facebook");
            elseif ($item['type'] == "x-im/social+twitter")
                $one_link->addAttribute("type", "x-im/social+twitter");
            elseif ($item['type'] == "Gravatar")
                $one_link->addAttribute("type", "x-im/social+gravatar");
            elseif ($item['type'] == "x-np/user")
                $one_link->addAttribute("type", "x-np/user");
            elseif ($item['type'] == "x-ad/sam_account_name")
                $one_link->addAttribute("type", "x-ad/sam_account_name");
            elseif ($item['type'] == "text/html")
                $one_link->addAttribute("type", "text/html");
            $one_link->addAttribute("url", $item['link']);
        }
    }

    //concept block
    $concept = $xml->concept[0];
    //name
    $xml->concept[0]->name = $name;
    //type
    $xml->concept[0]->type['qcode'] = "cpnat:" . getTypeValue($type);
    //long descprition
    $xml->concept[0]->definition[0] = $description_long;
    //short description
    $xml->concept[0]->definition[1] = $description_short;
    if ($type == "author") {
        $data = $xml->concept[0]->metadata->object->data;
        if (count($data) > 0) {
            $data->email = $email;
            $data->phone = $phone;
            $data->streetAddress = $streetadress;
            $data->postalCode = $postalcode;
            $data->country = $country;
        } else {
            $key = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 11);
            $metadata = $concept->addChild('metadata');
            $metadata->addAttribute('xmlns', 'http://www.infomaker.se/newsml/1.0');
            $object = $metadata->addChild('object');
            $object->addAttribute('id', $key);
            $object->addAttribute('type', 'x-im/contact-info');
            $data = $object->addChild('data');
            $data->addChild('email', $email);
            $data->addChild('phone', $phone);
            $data->addChild('streetAddress', $streetadress);
            $data->addChild('postalCode', $postalcode);
            $data->addChild('country', $country);
            $fullname = explode(" ", $name);
            if ($fullname[0]) {
                $xml->itemMeta[0]->itemMetaExtProperty[1]['value'] = $fullname[0];
            }
            if ($fullname[1]) {
                $xml->itemMeta[0]->itemMetaExtProperty[2]['value'] = $fullname[1];
            }

        }
    } else {
        if ($type !== "organisation" && count($concept->metadata) > 0) {
            unset($concept->metadata);
        }
    }

    $xml->asXML($target_dir . $xml_filename);

    return $xml_filename;

}


/**
 * Create XML file for proposals
 * @param $guid
 * @param $name
 * @param $type
 * @param $description_long
 * @param $description_short
 * @param $target_dir
 * @param $note
 * @param $links
 * @return string
 */
function getTypeValue($type)
{
    if ($type == "author" || $type == "person")
        return "person";
    if ($type == "topic")
        return "object";
    if ($type == "place")
        return "poi";
    if ($type == "story")
        return "story";
    if ($type == "organisation")
        return "organisation";
    if ($type == "content-profile")
        return "object";
    if ($type == "category")
        return "abstract";
}

/**
 * Clean filename on save
 * @param $input
 * @return mixed
 */
function clean_filename($input)
{
    $input = preg_replace("/[^a-zA-Z0-9]/", "", $input);
    return $input;
}

/**
 * Generate GUID
 * @param bool $opt
 * @return string
 */
function getGUID($opt = true)
{
    if (function_exists('com_create_guid')) {
        if ($opt) {
            return com_create_guid();
        } else {
            return trim(com_create_guid(), '{}');
        }
    } else {
        mt_srand((double)microtime() * 10000);
        $charid = md5(uniqid(rand(), true));
        $hyphen = chr(45);    // "-"
        $uuid = substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4) . $hyphen
            . substr($charid, 12, 4) . $hyphen
            . substr($charid, 16, 4) . $hyphen
            . substr($charid, 20, 12);
        return $uuid;
    }
}

/**
 * Upload standard XML file to API
 * @param $xml_filename
 * @param $xmlFile
 */
function upload_to_oc($xml_filename, $xmlFile, $guid)
{
    $curl_data = array(
        'file' => $guid,
        $guid => $xmlFile,
        'id' => $guid,
        'batch' => 'false',
        'file-mimetype' => "application/vnd.iptc.g2.conceptitem+xml",
        'metadata' => $guid,
        $guid => $xmlFile,
        'metadata-mimetype' => "application/vnd.iptc.g2.conceptitem+xml"
    );
    curl_to_upload_to_oc($curl_data);
}

/**
 * Upload to OC
 * @param $curl_data
 */
function curl_to_upload_to_oc($curl_data)
{
    $headers = array("Content-Type:multipart/form-data");
    $url = get_oc_api_base_path() . "/objectupload";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERPWD, get_oc_api_credentials());
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_data);
    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
    curl_exec($ch);
    $returnvalue = curl_getinfo($ch);
    curl_close($ch);
    print_r($returnvalue);
}

/**
 * GET data from API
 * @param $url
 * @param $credentials
 * @param string $returnvalue
 * @param string $method
 * @return mixed
 */
function get_api_data($url, $credentials, $method = "GET")
{
    //print "<b>URL: </b><a href=\"$url\" target=\"_blank\">" .$url ."</a><br><br>";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json; charset=UTF-8'));
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_USERPWD, $credentials);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $data = curl_exec($ch);

    curl_close($ch);
    return $data;
}

/**
 * Check tag status
 * @param $status
 * @return string
 */
function get_tagstatus_class($status)
{
    if ($status == "withheld")
        return "btn-warning";
    else if ($status == "cancelled")
        return "btn-danger";
    else if ($status == "usable")
        return "btn-success";
    else
        return "";
}

function get_tagstatus_text($status)
{
    if ($status == "withheld")
        return "Väntar";
    else if ($status == "cancelled")
        return "Spärrad";
    else if ($status == "usable")
        return "Godkänd";
    else
        return "";
}

function get_next_tagstatus($status)
{
    if ($status == "withheld")
        return "usable";
    if ($status == "usable")
        return "cancelled";
    if ($status == "cancelled")
        return "withheld";
    return "";
}
