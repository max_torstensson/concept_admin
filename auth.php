<?php

/**
 * API-settings OpenContent
 * @return string
 */
function get_base_path()
{
    return "http://52.49.83.6:8080/opencontent";
}

function get_credentials()
{
    return "admin:wacky47*lows";
}

/**
 * API-settings Newspilot
 * @return string
 */
function get_np_base_path()
{
    return "http://172.22.0.84:8080/webservice"; // 172.22.0.80: skarpt API, 172.22.0.84 Test-API
}

function get_np_credentials()
{
    // Get credentionals from javascript from current user
    return $_GET['user'] . ":" . $_GET['passw'];
}

