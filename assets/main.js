/**
 *  Base64 encode / decode
 *  http://www.webtoolkit.info/
 **/

var Base64 = {

    // private property
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            }
            else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
            this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
            this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode: function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}
var delete_tag = {
    /**
     * Delete tag from API
     */
    del: function()
    {
        $('body').on('click', '#removetag', function (e) {
            var id = $("#tagid").val();
            e.preventDefault();
            if (confirm("Vill du verkligen ta bort taggen?")) {
                $.get('update_data.php?action=deletetag&id=' + id, function (data) {
                    window.clearTimeout($(this).data("timeout"));
                    $(this).data("timeout", setTimeout(function () {
                        search.search_tags(500);
                        $('#message').html($message_removed);
                        $("#editmodal").modal('hide');
                    }, 1000));
                });
            }
        });
    }
}
var editadd = {
    /**
     * Edit tag
     */
    tempData: [],
    edit_tag: function (selectedtagid) {
        helper.reset_message();
        var guid = selectedtagid;
        var currentbtn = $(this);

        // Show modal
        $("#editmodal").modal('show');

        // Change hash
        window.location.hash = "#edit/" + guid;

        // Get information about the current clicked tag
        $.get('get_data.php?action=gettag&id=' + guid, function (data) {

            // Reset all icons
            $('.btnEdit').each(function () {
                $(this).html("<i class=\"fa fa-pencil\"></i>");
                $(this).removeClass("btn-default");
                $(this).addClass("btn-primary");
            });

            // Show remove link
            $("#removetag").show();

            // Headline
            $("#formtitle").html("Redigera tagg");

            // Proposals
            var prop_name = editadd.look_for_value("ConceptProposalName", data.properties);

            var prop_username = editadd.look_for_value("ConceptProposalUser", data.properties);
            var prop_type = editadd.look_for_value("ConceptProposalType", data.properties);
            var prop_long = editadd.look_for_value("ConceptProposalDefinitionLong", data.properties);
            var prop_short = editadd.look_for_value("ConceptProposalDefinitionShort", data.properties);
            var prop_note = editadd.look_for_value("ConceptProposalNote", data.properties);
            var prop_links = editadd.look_for_value("ConceptProposalSeeAlso", data.properties);
            if (prop_name !== undefined || prop_type !== undefined || prop_long !== undefined || prop_short !== undefined || prop_note !== undefined || prop_links !== undefined) {
                $("#proposal-fields").show();

                if (prop_name !== undefined) {
                    $("#proposal_tagname").val(prop_name.values);
                }
                else
                    $("#proposal_tagname").val("");

                if (prop_type !== undefined)
                    $("#proposal_tagtype").val(prop_type.values);
                else
                    $("#proposal_tagtype").val("");

                if (prop_long !== undefined)

                    $("#proposal_description_long_admin").val(prop_long.values[0]);
                else
                    $("#proposal_description_long_admin").val("");

                if (prop_short !== undefined)
                    $("#propsal_description_short").val(prop_short.values);
                else
                    $("#propsal_description_short").val("");

                if (prop_note !== undefined)
                    $("#proposal_Note_admin").val(prop_note.values[0]);
                else
                    $("#proposal_Note_admin").val("");

                if (prop_links !== undefined) {
                    var str_links = prop_links.values.toString();
                    $("#propsal_links").html(str_links);
                }
                else
                    $("#propsal_links").html("");

                if (prop_username.values[0] !== undefined) {
                    $("#proposal_username_header").html(" Inskickat av " + prop_username.values[0] + "");
                    $("#proposal_user").val(prop_username);
                } else {
                    $("#proposal_user").val("");
                    $("#proposal_username_header").html("");
                }
            }
            // Type

            var type = editadd.look_for_value("ConceptImType", data.properties);
            if (type !== undefined) {
                $("#tagtype").val(type.values[0]);

            }
            $("#tagtype option:not(:selected)").prop("disabled", true);
            if (type.values[0] == "author") {
                $(".author").show();
                $(".name").hide();
                $('#tagname').prop('required', false);
            }
            else {
                $(".author").hide();
                $(".name").show();
                $('#tagname').prop('required', true);
            }
            if (type.values[0] == "person" || type.values[0] == "author") {
                $(".person").show();
                $(".name").hide();
                $('#tagname').prop('required', false);
            }
            else {
                $(".person").hide();
                $(".name").show();
                $('#tagname').prop('required', true);
            }
            // First created
            var firstcreated = editadd.look_for_value("created", data.properties);
            if (firstcreated !== undefined)
                $("#date_created").val(firstcreated.values[0]);

            // Status. If we have ?user=tagadmin, set status to value from OC, otherwise set status to withheld
            var status = "";
            if (helper.getUrlVars()["user"] == undefined)
                status = "withheld";
            else
                status = editadd.look_for_value("ConceptStatus", data.properties).values[0];
            if (status !== undefined)
                $("#tagstatus").val(status);

            // Note
            var Note = editadd.look_for_value("ConceptNote", data.properties);
            if (Note !== undefined)
                $("#Note").val(Note.values[0]);
            // Name
            var tagname = editadd.look_for_value("ConceptName", data.properties);
            if (tagname !== undefined) {
                $("#tagname").val(tagname.values[0]);
            }
            //firstname
            var firstname = editadd.look_for_value("ConceptFirstName", data.properties);
            if (firstname !== undefined)
                $("#firstname").val(firstname.values[0]);
            //lastname
            var lastname = editadd.look_for_value("ConceptLastName", data.properties);
            if (lastname !== undefined)
                $("#lastname").val(lastname.values[0]);
            // Tag id
            $("#tagid").val(guid);

            // Links
            form.trashFormData();
            var links = editadd.look_for_value("ConceptLinkValueAndType", data.properties);
            //vill kunna hämta en property med en en länk och en typ.
            if (links !== undefined) {

                $(".input_fields_wrap").empty();
                for (i = 0; i < links.values.length; i++) {
                    if (links.values[i] !== "::" || links.values[i] !== "") {
                        var arr = links.values[i].split("::");
                        if (arr[1] !== "") {
                            //form.populatelinks(arr[1], arr[0]);
                            editadd.tempData.push(arr[1]);
                            form.addOneLink(arr[1]);
                        }
                    }
                }

            }
            else {
                $(".input_fields_wrap").empty();
                $(".addedlinks").hide();
            }

            // Long description
            var longdesc = editadd.look_for_value("ConceptDefinitionLong", data.properties);
            if (longdesc !== undefined)
                $("#description_long").val(longdesc.values[0]);
            else
                $("#description_long").val("");

            // Short description
            var shortdesc = editadd.look_for_value("ConceptDefinitionShort", data.properties);
            if (shortdesc !== undefined)
                $("#description_short").val(shortdesc.values[0]);
            else
                $("#description_short").val("");

            // e-mail
            var email = editadd.look_for_value("ConceptEmail", data.properties);
            if (email !== undefined) {
                $("#email").val(email.values[0]);
            }
            //Phone
            var phone = editadd.look_for_value("ConceptPhone", data.properties);
            if (phone !== undefined) {
                $("#phone").val(phone.values[0]);
            }
            //streetAddress
            var streetAddress = editadd.look_for_value("ConceptStreetAddress", data.properties);
            if (streetAddress !== undefined) {
                $("#streetAddress").val(streetAddress.values[0]);
            }
            //postalcode
            var postalCode = editadd.look_for_value("ConceptPostalCode", data.properties);
            if (postalCode !== undefined) {
                $("#postalCode").val(postalCode.values[0]);
            }
            //Country
            var country = editadd.look_for_value("ConceptCountry", data.properties);
            if (country !== undefined) {
                $("#country").val(country.values[0]);
            }

        }, "json");

    },

    /**
     * Look for values in properties
     * @param key
     */
    look_for_value: function (key, properties) {
        for (var i = 0; i < properties.length; i++) {
            if (properties[i].name == key)
                return properties[i];
        }
        return undefined;
    },


    /**
     * Add tag
     */
    add_tag: function () {
        helper.reset_message();

        // Show modal
        $("#editmodal").modal('show');

        // Change hash
        window.location.hash = "#add";

        //lock upp type
        $("#tagtype option:not(:selected)").prop("disabled", false);


        // Hide propsals
        $("#proposal-fields").hide();

        // Headline
        $("#formtitle").html("Lägg till tagg");

        // Type

        $("#tagtype").val("");


        // First created
        $("#date_created").val("");

        // Status
        $("#tagstatus").val("");

        // Name
        $("#tagname").val("");

        $(".person").hide();

        //firstname
        $("#firstname").val("");
        //lastname
        $("#lastname").val("");

        // Tag id
        $("#tagid").val("");

        // Note
        $("#Note").val("");

        // Links
        $(".input_fields_wrap").empty();

        // Long description
        $("#description_long").val("");

        // Short description
        $("#description_short").val("");
        //e-mail
        $("#email").val("");
        //phone
        $("#phone").val("");
        //Street adress
        $("#streetAddress").val("");
        //Postal code
        $("#postalCode").val("");
        //country
        $("#country").val("");
    },

    /**
     * Event handling for edit and add function
     */
    edit_event: function () {
        $('body').on('click', '.btnEdit', function () {
            editadd.edit_tag($(this).data("id"));

        });

        $('#editmodal').on('hidden.bs.modal', function (e) {
            editadd.tempData = [];
            $('.link').val("");
            $('#ErrorMessagePopUp').hide();
        });

        $('body').on('click', '#createtag', function () {

            $(".author").hide();
            $(".addedlinks").hide();
            editadd.add_tag();
        });
        $('body').on('click', '.showedit', function () {
            editadd.show_edit_tag($(this).data("tagid"));
        });
    },


    /**
     * Shoew edit tag information and proposals in newspilot
     */
    show_edit_tag: function (selectedtagid) {
        helper.reset_message();
        var guid = selectedtagid;
        var currentbtn = $(this);

        // Show modal
        $("#showedit").modal('show');

        // Get information about the current clicked tag
        $.get('get_data.php?action=gettag&id=' + guid, function (data) {


            // Clean up proposal information
            $("#proposal_description_long").val("");
            $("#proposal_description_short").val("");
            $("#proposal_Note").val("");
            //Mail

            //username

            // Type
            var type = editadd.look_for_value("ConceptType", data.properties);
            if (type !== undefined) {
                $("#show_tagtype").val(type.values[0]);
                $("#proposal_tagtype").val(type.values[0]);

            }

            // First created
            var firstcreated = editadd.look_for_value("created", data.properties);
            if (firstcreated !== undefined)
                $("#show_date_created").val(firstcreated.values[0]);

            // Status. If we have ?user=tagadmin, set status to value from OC, otherwise set status to withheld
            var status = "";
            if (helper.getUrlVars()["user"] == undefined)
                status = "withheld";
            else
                status = editadd.look_for_value("ConceptStatus", data.properties).values[0];
            if (status !== undefined)
                $("#show_tagstatus").val(status);

            // Note
            var Note = editadd.look_for_value("ConceptNote", data.properties);
            if (Note !== undefined) {
                $("#show_Note_info").val(Note.values[0]);
                $("#show_Note").val(Note.values[0]);
            }

            // Name
            var tagname = editadd.look_for_value("ConceptName", data.properties);
            if (tagname !== undefined) {
                $("#show_tagname").val(tagname.values[0]);
                $("#show_tagname_info").val(tagname.values[0]);

            }

            // Tag id
            $("#show_tagid").val(guid);

            // Links
            $(".input_fields_wrap").empty();
            var links = editadd.look_for_value("ConceptLinkValueAndType", data.properties);
            for (i = 0; i < links.length; i++) {
                console.log(links[i]);
            }

            var str_links = "";
            if (links !== undefined)
                str_links = links.values.toString();
            $("#show_links").html(str_links);

            // Long description
            var longdesc = editadd.look_for_value("ConceptDefinitionLong", data.properties);
            if (longdesc !== undefined) {
                $("#show_description_long_info").val(longdesc.values[0]);
                $("#show_description_long").val(longdesc.values[0]);
            }
            else {
                $("#show_description_long_info").val("");
                $("#show_description_long").val("");
            }

            // Short description
            var shortdesc = editadd.look_for_value("ConceptDefinitionShort", data.properties);
            if (shortdesc !== undefined) {
                $("#show_description_short_info").val(shortdesc.values[0]);
                $("#show_description_short").val(shortdesc.values[0]);
            }
            else {
                $("#show_description_short_info").val("");
                $("#show_description_short").val("");
            }
        }, "json");
    },

    /**
     * Change status of tag
     */
    change_tag_status: function () {
        $('body').on('click', '.btnStatus', function () {
            var id = $(this).data("id");
            var currentstatus = $(this).data("currentstatus");
            var currentbtn = $(this);

            // Update data in API
            $.get('update_data.php?action=updatestatus&id=' + id + "&current=" + currentstatus, function (data) {
                // Change status of buttons
                $("#status_" + id).removeClass("btn-warning").removeClass("btn-danger").removeClass("btn-success");
                $("#status_" + id).addClass(helper.get_next_status(currentstatus, 0));
                $("#status_" + id).html(helper.get_next_status(currentstatus, 1));
                currentbtn.data("currentstatus", helper.get_next_status(currentstatus, 2));
                search.search_tags(500);
            });
        });
    }
}
var form = {
    /**
     * Submit form with ajax
     */

    formData: [],
    RemovedsLinks: [],
    submitform: function () {

        var self = this;
        console.log(self.formData);
        if (!form.checkform()) {
            $('#ErrorMessagePopUp').html($message_error_update);
            $("#loginiconform").hide();
            $("#tagname").focus();
            return false;
        }
        var tagform = $("#tagform"),
            tagid = $("#tagid").val(),
            data = tagform.find(':input:not([name="links"])').serialize();
        var form_action = tagid == "" ? "add_data.php" : "update_data.php?action=updatetag&id=" + tagid;
        $("#loginiconform").show();
        console.log(data);
        $.ajax({
            type: "POST",
            url: form_action,
            data: {data: data, formData: self.formData, removedlinks: self.RemovedsLinks},
            success: function () {
                window.clearTimeout($(this).data("timeout"));
                $(this).data("timeout", setTimeout(function () {
                    if (typeof $newspilot !== 'undefined') {
                        // Get information about the recent added tag
                        $.get('get_data.php?action=gettag_by_name&search=' + $("#tagname").val() + "&start=0&limit=1&type=all&sorting=name", function (data) {
                            newspilot.add_tag(data.id, data.versions[0].properties.ConceptName[0], data.versions[0].properties.ConceptStatus[0]);
                            $("#editmodal").modal('hide');
                        }, "json");
                    } else {
                        this.search.search_tags(500);
                        $("#editmodal").modal('hide');
                        $('#message').html($message_success_update);
                        $("#loginiconform").hide();
                    }
                }, 1000));
            }
        });
        self.formData = [];
        return false;
    },

    /**
     * Submit showedit newspilot form
     */
    showedit_submit: function () {
        var tagform = $("#show_tagform"),
            tagid = $("#show_tagid").val(),
            formData = [],
            data = tagform.find(':input:not([name="links"])').serialize();
        tagform.find('input[name="links"]').each(function () {
            formData.push(this.value);
        });
        data += '&links=' + formData.join(',');

        var form_action = "update_data.php?action=updateproposal&id=" + tagid + "&user=" + variables.$newspilot_current_user;
        $("#loginiconform").show();
        $.ajax({
            type: "POST",
            url: form_action,
            data: data,
            success: function () {
                window.clearTimeout($(this).data("timeout"));
                $(this).data("timeout", setTimeout(function () {
                    $("#showedit").modal('hide');
                    $("#loginiconform").hide();
                }, 1000));
            }
        });
        return false;
    },

    /**
     * Check form values
     */
    checkform: function () {
        /*
         if ($("#tagname").val() === "")
         return false;
         */
        if ($("#tagtype").val() === "")
            return false;
        return true;
    },

    /**
     * Click to show more links
     */
    populatelinks: function ($link, $type) {
        var self = this;
        self.formData.push({link: $link, type: $type})


    },
    trashFormData: function () {
        var self = this;
        self.formData = [];
    },

    addlinks: function () {
        var wrapper = $(".input_fields_wrap");
        var linkform = $(".add_links_form");
        //var add_button = $(".add_field_button");
        var self = this;
        //on add input button click
        $(linkform).append('<div class="tag-row">' +
            '<div class="form-inline">' +
            '<input type="text" class="form-control link" name="links">' +
            '<select class="selectpicker form-control" id="seltypelink" title="Please select a type">' +
            '<option value="x-im/social+facebook">Facebook</option> ' +
            '<option value="x-im/social+twitter">Twitter</option> + ' +
            '<option>Gravatar</option> ' +
            '<option value="x-np/user">Newspilot</option> ' +
            '<option value="x-ad/sam_account_name">AD</option> ' +
            '<option value="text/html">Övrigt</option> ' +
            '</select>' +
            '<a class="btn btn-success" id="addlink"><i class="fa fa-plus"></i></a>' +
            '</div>');
        $(wrapper).on("click", ".remove_field", function (e) {
            e.preventDefault();
            $value = $(this).siblings().val();
            var index = editadd.tempData.indexOf($value);
            editadd.tempData.splice(index, 1);
            //remove from array
            self.RemovedsLinks.push($value);
            form.RemoveFormDataItem($value);
            $(this).parent('div').remove();
            $linksitem = $('.links').length;
            if ($linksitem == 0) {
                $('.addedlinks').hide();
            }
            var bool = form.identifyDuplicatesFromArray(editadd.tempData);
            if (bool == false) {
                helper.reset_message();
                $("#ErrorMessagePopUp").hide();
            }
        });
        $(linkform).on("click", "#addlink", function (e) {
            var link = $('.link').val();
            var bool = form.isExist(editadd.tempData, link);
            if (bool == true) {
                $('#ErrorMessagePopUp').show();
                $('#ErrorMessagePopUp').html($message_doublets_links);
            }
            if (bool !== true) {
                editadd.tempData.push(link);
                var seltyp = $('#seltypelink :selected').val();
                form.populatelinks(link, seltyp);
                $('.link').val("");
                form.addOneLink(link);
            }
            // console.log(self.formData);
        });
        $("#addbtn").click(function () {
            $(".showlinks").toggle();
        });
    },
    isExist: function (arr, str) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === str) {
                // Return index
                return true;
            } else {
                /* If value is not found return -1. */
                return false;
            }
        }
    },
    identifyDuplicatesFromArray: function (arr) {
        var i;
        var len = arr.length;
        var obj = {};
        var duplicates = [];
        for (i = 0; i < len; i++) {
            if (!obj[arr[i]]) {
                obj[arr[i]] = {};
            }
            else {
                return true
            }
        }
        return false;
    },
    RemoveFormDataItem: function (itemName) {
        var self = this;
        for (var i = 0; i < self.formData.length; i++) {
            if (self.formData[i].link == itemName) {
                self.formData.splice(i, 1);
                break;
            }
        }
    },
    /**
     * Add one link from edit form
     * @param url
     */
    addOneLink: function (url) {

        $(".input_fields_wrap").append('' +
            '<div class="tag-row">' +
            '<div class="form-inline">' +
            '<input type="text" class="form-control links" name="links" value="' + url + '" disabled>' +
            ' <a href="#" class="btn btn-default remove_field"><i class="fa fa-trash"></i></a>' +
            '</div></div>');

        //add label
        $('.addedlinks').show();
    },

    /**
     * Empty poposals fields
     */
    empty_proposals: function () {
        $("#proposal_tagname").val("");
        $("#proposal_tagtype").val("");
        s
        $("#proposal_description_long_admin").val("");
        $("#propsal_description_short").val("");
        $("#proposal_Note_admin").val("");
        $("#propsal_links").html("");
    }
}
var hash = {
    /**
     * Listen to hash changes from user
     */
    init: function() {
        var hashvalue = window.location.hash.replace("#",'');

        // Edit
        if(hashvalue !== undefined) {
            if (hashvalue.length > 5) {
                if (hashvalue.substring(0, 4) == "edit") {
                    editadd.edit_tag(hashvalue.substring(5));
                }
            }
        }
    }
}
var helper = {
    /**
     * Reset message text
     */
    reset_message: function()
    {
        $('#message').html("");
    },

    /**
     * Get current selected status
     */
    get_current_status: function() {
        var status = "";
        $(".status_button").each(function () {
            if ($(this).hasClass("active"))
                status += "ConceptStatus:" + $(this).data("status") + " OR ";
        });
        status = "(" + status.substring(0, status.length - 4) + ")";
        return status;
    },

    /**
     * Get current selected search sorting
     */
    get_current_sorting: function() {
        return $("#search_sorting").val();
    },

    /**
     * Get current selected type for search
     */
    get_current_type: function() {
        return $("#search_type").val();
    },

    /**
     * Set title of table
     * @param header
     * @param no_of_tags
     */
    set_title: function(header, no_of_tags) {
        if(no_of_tags / $paging_items <= 1){
            $("#tagTitle").html(header + $("#search").val() + " <small>" + no_of_tags + " taggar (Sida " + ($current_page + 1) + " av  1)</small>");
        }
        if(no_of_tags / $paging_items == 0){
            $("#tagTitle").html(header + $("#search").val() + " <small>" + no_of_tags + " taggar (Sida " + ($current_page + 1) + " av 0  )</small>");
        }
        else{
            $("#tagTitle").html(header + $("#search").val() + " <small>" + no_of_tags + " taggar (Sida " + ($current_page + 1) + " av " + Math.ceil(no_of_tags / $paging_items) + ")</small>");
        }
    },

    /**
     * Return all variables from querystring
     * @returns {Array}
     */
    getUrlVars: function()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },

    /**
     * Return _next_ status of a tag
     * @param currentstatus
     * @param returnvalue 0 = class, 1 = text, 2 = machinetext
     * @returns {string}
     */
    get_next_status: function(currentstatus, returnvalue) {
        if (currentstatus === "withheld") {
            if (returnvalue == 0)
                return "btn-success";
            else if (returnvalue == 1)
                return "Godkänd";
            else
                return "usable";
        } else if (currentstatus === "usable") {
            if (returnvalue == 0)
                return "btn-danger";
            else if (returnvalue == 1)
                return "Spärrad";
            else
                return "cancelled";
        } else if (currentstatus === "cancelled") {
            if (returnvalue == 0)
                return "btn-warning";
            else if (returnvalue == 1)
                return "Väntar";
            else
                return "withheld";
        } else
            return "";
    }
}
// https://incident57.com/codekit/help.html#javascript
// @codekit-prepend "variables.js";
// @codekit-prepend "edit.js";
// @codekit-prepend "search.js";
// @codekit-prepend "paging.js";
// @codekit-prepend "form.js";
// @codekit-prepend "helper.js";
// @codekit-prepend "delete.js";
// @codekit-prepend "hash.js";
// @codekit-prepend "newspilot.js";
// @codekit-prepend "base64.js";

$(function () {
    // Init variables
    variables.init();

    // Focus search box on start and search for tags when a user type in a search textbox
    if (document.getElementById("search") !== null) {
        $("#search").focus().bind("input propertychange", function (evt) {
            if (window.event && event.type == "propertychange" && event.propertyName != "value")
                return;
            search.search_tags();
        });
    }
    $(".btn").mouseup(function(){
        $(this).blur();
    })
    // Events for edit, delete tag and change tag status
    editadd.edit_event();
    delete_tag.del();
    editadd.change_tag_status();
    //
    $("#search").keypress(function(e){
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            return false;
        }
    });

    // Click on status buttons
    $(".status_button").click(function () {
        search.search_tags();
    });

    // Change sorting selectbox
    $("#search_sorting").change(function () {
        search.search_tags();
    });

    // Change type selectbox
    $("#search_type").change(function () {
        search.search_tags();
    });
    var type = $("#tagtype").val();
    if (type == "author") {
        $(".author").show();
    }
    else {
        $(".author").hide();
    }
    if (type == "person" || type == "author") {
        $(".person").show();
        $(".name").hide();
        $('#tagname').attr("required", false);
    }
    else {
        $(".person").hide();
        $(".name").show();
        $('#tagname').attr("required", true);
    }
    $("#tagtype").change(function (e) {
        var type = $("#tagtype").val();
        if (type == "author") {
            $(".author").show();
        }
        else {
            $(".author").hide();
        }
        if (type == "person" || type == "author") {
            $(".person").show();
            $(".name").hide();
            $('#tagname').prop('required',false);
        }
        else {
            $(".person").hide();
            $(".name").show();
            $('#tagname').prop('required',true);
        }
    });
    // Submit form
    $(".submitbutton").click(function (e) {
        e.preventDefault();
        form.submitform();


    });
    $("#tagform").submit(function (e) {
        e.preventDefault();
        form.submitform();

    });


    // Submit form view/edit in newspilot
    $(".submitviewbutton").click(function (e) {
        e.preventDefault();
        form.showedit_submit();
    });
    $("#show_tagform").submit(function (e) {
        e.preventDefault();
        form.showedit_submit();
    });

    // Verification mode for all search filters
    $("#verifymode").click(function () {
        $("#search_sorting").val("date");
        $("#search_type").val("all");
        $("#status_withheld").addClass("active");
        $("#status_usable").removeClass("active");
        $("#status_cancelled").removeClass("active");
        search.search_tags();
    });

    // Button for empty proposals
    $('body').on('click', '.emptyproposals', function () {
        form.empty_proposals();
    });

    if (document.getElementById("search") !== null) {
        // Paging events
        paging.paging_event();

        // Initial search
        search.search_tags();
    }

    // Event handler for hash changes
    window.onload = function () {
        hash.init();
    };

    // Event handler for more link-buttons
    form.addlinks();

    // Events for newspilotpage
    if (document.getElementById("search_newspilot") !== null)
        newspilot.init();
});
var newspilot = {
    /**
     * Initiate search function and event handling for buttons
     */
    init: function() {
        // Focus search box on newspilot page and search for tags when a user type in a search textbox
        $("#search_newspilot").focus().bind("input propertychange", function (evt) {
            if (window.event && event.type == "propertychange" && event.propertyName != "value")
                return;
            search.search_tag_newspilot();
        });

        // Focus story search box on newspilot page and search for tags when a user type in a search textbox
        $("#search_newspilot_story").focus().bind("input propertychange", function (evt) {
            if (window.event && event.type == "propertychange" && event.propertyName != "value")
                return;
            search.search_story_newspilot();
        });

        // Check if view/edit-dialog is shown
        $('#showedit').on('show.bs.modal', function (e) {
            variables.$newspilot_vieweditdialog = "true";
        });
        $('#showedit').on('hide.bs.modal', function (e) {
            variables.$newspilot_vieweditdialog = "false";
        });

        // Add tag to collection
        $('body').on('click', '.addtocollection', function () {
            newspilot.add_tag($(this).data('tagid'), $(this).data('tagname'), $(this).data('tagstatus'));
        });

        // Remove tag from collection
        $('body').on('click', '.removefromcollection', function () {
            newspilot.delete_tag($(this).data('tagid'));
        });

        // Add story to collection
        $('body').on('click', '.addtocollection_story', function () {
            newspilot.add_story($(this).data('tagid'), $(this).data('tagname'), $(this).data('tagstatus'));
        });

        // Remove tag from collection
        $('body').on('click', '.removefromcollection_story', function () {
            newspilot.delete_story($(this).data('tagid'));
        });

        // Get values from newspilot
        variables.$newspilot_article_id = getNewspilotArticleIdAsString();
        variables.$newspilot_current_user = GetNewspilotLogin();
        variables.$newspilot_current_user_passw = GetNewspilotPassword();

        if(variables.$newspilot_article_id === "" || variables.$newspilot_article_id === undefined) {
            $('#debug').html("<b>Fel på koppling till newspilot. Kontakta IT-support.</b>")
        } else {
            // Get tags and stories from from selected article
            $.get('get_data_newspilot.php?function=articletags&articleid=' + variables.$newspilot_article_id +'&user=' + variables.$newspilot_current_user +'&passw=' + variables.$newspilot_current_user_passw, function (data) {
                // data[0] = tags, data[1] = original userdata, data[2] = etag, data[3] = articleId, data[4] = stories

                if($data[0] != null) {
                    $("#notags").remove();
                    for (var i = 0; i < data[0].length; i++) {
                        $("#newspilot_collection").prepend("<tr class=\"tag_in_collection\" data-tagid=\"" + data[0][i].id + "\"><td><button class=\"btn " + newspilot.get_tagstatus_class(data[0][i].versions[0].properties.ConceptStatus[0]) + " btn-circle btn-xs\" type=\"button\"> </button> " + data[0][i].versions[0].properties.ConceptName[0] + "</td><td><a class=\"removefromcollection\"data-tagid=\"" + data[0][i].id + "\"><i class=\"fa fa-trash\"></i></a></td></tr>");
                    }
                }
                if($data[4] != null) {
                    $("#nostories").remove();
                    for (var i = 0; i < data[4].length; i++) {
                        $("#newspilot_stories_collection").prepend("<tr class=\"story_in_collection\" data-tagid=\"" + data[4][i].id + "\"><td><button class=\"btn " + newspilot.get_tagstatus_class(data[4][i].versions[0].properties.ConceptStatus[0]) + " btn-circle btn-xs\" type=\"button\"> </button> " + data[4][i].versions[0].properties.ConceptName[0] + "</td><td><a class=\"removefromcollection_story\"data-tagid=\"" + data[4][i].id + "\"><i class=\"fa fa-trash\"></i></a></td></tr>");
                    }
                }
                variables.$newspilot_userdata = data[1];
                variables.$newspilot_etag = data[2];
                variables.$newspilot_article_id = data[3];

            }, "json");
        }
    },

    /**
     * Edit user data on article
     * @returns {boolean}
     */
    edit_userdata: function() {
        // String with uuid:s of articletags
        var tag_uuids = "";
        $(".tag_in_collection").each(function () {
            tag_uuids += $(this).data("tagid") +",";
        });
        tag_uuids = tag_uuids.slice(0, -1);

        // String with uuid:s of stories
        var story_uuids = "";
        $(".story_in_collection").each(function () {
            story_uuids += $(this).data("tagid") +",";
        });
        story_uuids = story_uuids.slice(0, -1);

        data = "etag=" +variables.$newspilot_etag +"&userdata=" +variables.$newspilot_userdata +"&tags=" +tag_uuids +"&stories=" +story_uuids;

        // Post information to backend
        $.ajax({
            type: "POST",
            url: "update_data.php?action=updateUserData&id=" +variables.$newspilot_article_id  +'&user=' + variables.$newspilot_current_user +'&passw=' + variables.$newspilot_current_user_passw,
            data: data,
            success: function (data) {
                return true;
            }
        });
        return false;
    },

    /**
     * Add tag to collection
     */
    add_tag: function(id, name, status) {
        if(!newspilot.tag_in_collection(id)) {
            $("#notags").remove();
            $("#newspilot_collection").prepend("<tr class=\"tag_in_collection\" data-tagid=\"" + id + "\"><td><button class=\"btn " + newspilot.get_tagstatus_class(status)  +" btn-circle btn-xs\" type=\"button\"> </button> " + name + "</td><td><a class=\"removefromcollection\"data-tagid=\"" + id + "\"><i class=\"fa fa-trash\"></i></a></td></tr>");
        }
    },

    /**
     * Delete tag from collection
     */
    delete_tag: function(id) {
        $(".tag_in_collection").each(function () {
            if (String($(this).data("tagid")) == String(id))
                $(this).remove();
        });
    },

    /**
     * Add story to collection
     */
    add_story: function(id, name, status) {
        if(!newspilot.story_in_collection(id)) {
            $("#nostories").remove();
            $("#newspilot_stories_collection").prepend("<tr class=\"story_in_collection\" data-tagid=\"" + id + "\"><td><button class=\"btn " + newspilot.get_tagstatus_class(status)  +" btn-circle btn-xs\" type=\"button\"> </button> " + name + "</td><td><a class=\"removefromcollection_story\" data-tagid=\"" + id + "\"><i class=\"fa fa-trash\"></i></a></td></tr>");
        }
    },

    /**
     * Delete story from collection
     */
    delete_story: function(id) {
        $(".story_in_collection").each(function () {
            if (String($(this).data("tagid")) == String(id))
                $(this).remove();
        });
    },

    /**
     * Check if a tag is in collection
     * @param id
     */
    tag_in_collection: function(id) {
        $(".tag_in_collection").each(function () {
            if (String($(this).data("tagid")) == String(id))
                return true;
        });
        return false;
    },

    /**
     * Check if a story is in collection
     * @param id
     */
    story_in_collection: function(id) {
        $(".story_in_collection").each(function () {
            if (String($(this).data("tagid")) == String(id))
                return true;
        });
        return false;
    },

    /***
     * Return button class for status
     * @param status
     * @returns {string}
     */
    get_tagstatus_class: function(status) {
        if(status == "withheld")
            return "btn-warning";
        else if(status == "cancelled")
            return "btn-danger";
        else if(status == "usable")
            return "btn-success";
        else
            return "";
    }
};

/*
 Special functions for newspilotintegration, documentation: https://wiki.infomaker.se/display/NPM42SVE/Webbvy-funktioner https://wiki.infomaker.se/display/NPM42SVE/Fliken+Webbvyer+4.7
 */
function getOutputResult() {
    //if(variables.$newspilot_vieweditdialog == "false")
        return newspilot.edit_userdata();
    //else
    //    return "false";
}
function getNewspilotArticleIdAsString() {
    var selection = GetNewspilotSelection();

    var jsonstr = Base64.decode(selection);
    var arrayOfObjects = eval(jsonstr);
    var arrayAsString = "";


    for (var i = 0; i < arrayOfObjects.length; i++) {
        var object = arrayOfObjects[i];
        for (var property in object) {
            if(property == "id")
                arrayAsString += object[property];
        }
    }

    return arrayAsString;
}
var paging = {
    /**
     * Listen for paging events
     */
    paging_event: function () {
        $('body').on('click', '.pagingbutton', function () {
            paging.change_paging($(this).data('page'));
        });
    },

    /**
     * Change current page, make a new search and scroll up
     * @param page
     */
    change_paging: function (page) {
        $current_page = page;
        if (page <= -1 || page == null) {
            return;
        }
        search.search_tags();
        $("html, body").animate({scrollTop: 0}, "fast");

        // Change hash
        window.location.hash = "#page/" + page;
    },

    /**
     * Print paging form on the page
     */
    print_paging: function (total) {
        var pages = (total / $paging_items);
        var pagingcontent = "";
        if ($current_page > pages) {
            $current_page = 0;
            search.search_tags();
            paging.change_paging(0);
        }
        console.log($current_page);
        if ($current_page >= 1) {
            pagingcontent += "<button type=\"button\" data-page=\"" + 0 + "\" class=\"btn btn-default pagingbutton\"><i class='fa fa-fast-backward' aria-hidden='true'></i></button>";
            pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page - 1) + "\" class=\"btn btn-default btn-grey pagingbutton \"><i class='fa fa-step-backward' aria-hidden='true'></i></button>";
        }

        // Pages before current_page
        // Page buttons

        if ($current_page == 1) {
            for (i = 1; --i >= 0;) {
                pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page - i - 1) + "\" class=\"btn btn-default pagingbutton\">" + ($current_page - i) + "</button>";
            }
        }
        if ($current_page > 1) {
            for (i = 2; --i >= 0;) {
                pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page - i - 1) + "\" class=\"btn btn-default pagingbutton\">" + ($current_page - i) + "</button>";
            }
        }
        // Pages behind current_page
        // Current page
        pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page) + "\" class=\"btn btn-primary pagingbutton\">" + ($current_page + 1) + "</button>";

        for (i = 1; i < 3; i++) {
            if (($current_page + i) <= pages)
                pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page + i) + "\" class=\"btn btn-default pagingbutton\">" + ($current_page + i + 1) + "</button>";
        }


        // Pages before current_page
        if (pages > 1 && $current_page > 0 && pages.toFixed(0) > $current_page) {


            // Forward button

            pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page + 1) + "\" class=\"btn btn-default pagingbutton\"> <i class=\"fa fa-step-forward\"></i></button>";
        }

        // Set content
        $("#paging").html(pagingcontent);
    }
};
var search = {

    /**
     * Search function for tags
     */
    search_tags: function(timeoutvalue)
    {

        timeoutvalue = typeof timeoutvalue !== 'undefined' ? timeoutvalue : 200;
        window.clearTimeout($(this).data("timeout"));
        $(this).data("timeout", setTimeout(function () {
            $("#tagTable thead").show();
            var searchbox = $("#search").val();
            if (searchbox != "") {
                if (searchbox.length > 1) {
                    $("#loadingicon").show();
                    $.get('get_data.php?search=' + searchbox + "&start=" + $current_page + "&ConceptStatus=" + helper.get_current_status() + "&limit=" + $paging_items + "&sorting=" + helper.get_current_sorting() + "&type=" + helper.get_current_type(), function (data) {
                        $("#loadingicon").hide();
                        helper.set_title("Sökresultat: ", data[0]);
                        if (data[1] != "")
                            $("#tagTable tbody").html(data[1]);
                        else {
                            $("#tagTable thead").hide();
                            $("#tagTable tbody").html("<tr><td colspan='4'>Din sökning på <b>" + searchbox + "</b> returnerade inget resultat.</td></tr>");
                        }
                        paging.print_paging(data[0]);
                    }, "json");
                }
            } else {
                $.get('get_data.php?ConceptStatus=' + helper.get_current_status() + "&start=" + $current_page + "&limit=" + $paging_items + "&sorting=" + helper.get_current_sorting() + "&type=" + helper.get_current_type(), function (data) {
                    helper.set_title("Senaste skapade taggar ", data[0]);
                    $("#tagTable tbody").html(data[1]);
                    paging.print_paging(data[0]);
                }, "json");
            }
        }, timeoutvalue));
    },

    /**
     * Search for one tag for newspilot integration
     */
    search_tag_newspilot: function(timeoutvalue) {
        timeoutvalue = typeof timeoutvalue !== 'undefined' ? timeoutvalue : 200;
        window.clearTimeout($(this).data("timeout"));
        $(this).data("timeout", setTimeout(function () {
            var searchbox = $("#search_newspilot").val();
            if (searchbox != "") {
                if (searchbox.length > 1) {
                    $("#search_newspilot_result").hide();
                    $("#search_in_progress").show();
                    $.get('get_data_newspilot.php?function=tag&search=' + searchbox +'&type=all&limit=70&start=0&sorting=&user=' + variables.$newspilot_current_user +'&passw=' + variables.$newspilot_current_user_passw, function (data) {
                        $("#search_in_progress").hide();
                        $("#search_newspilot_result").show();
                        if (data != "")
                            $("#search_newspilot_result").html(data);
                        else {
                            $("#search_newspilot_result").html("<div class=\"infobox\"><div class=\"alert alert-info\" role=\"alert\">Taggen <b>" + searchbox + "</b> finns inte.<br><a id=\"createtag\" class=\"alert-link\">Vill du lägga till den?</a></div></div>");
                        }
                    }, "json");
                }
            }
        }, timeoutvalue));
    },


    search_story_newspilot: function(timeoutvalue) {
        timeoutvalue = typeof timeoutvalue !== 'undefined' ? timeoutvalue : 200;
        window.clearTimeout($(this).data("timeout"));
        $(this).data("timeout", setTimeout(function () {
            var searchbox = $("#search_newspilot_story").val();
            if (searchbox != "") {
                if (searchbox.length > 1) {
                    $("#search_newspilot_result_stories").hide();
                    $("#search_in_progress_story").show();
                    $.get('get_data_newspilot.php?function=story&search=' + searchbox +'&type=all&limit=70&start=0&sorting=&user=' + variables.$newspilot_current_user +'&passw=' + variables.$newspilot_current_user_passw, function (data) {
                        $("#search_in_progress_story").hide();
                        $("#search_newspilot_result_stories").show();
                        if (data != "")
                            $("#search_newspilot_result_stories").html(data);
                        else {
                            $("#search_newspilot_result_stories").html("<div class=\"infobox\"><div class=\"alert alert-info\" role=\"alert\">Storyn <b>" + searchbox + "</b> finns inte.<br><a id=\"createtag\" class=\"alert-link\">Vill du lägga till den?</a></div></div>");
                        }
                    }, "json");
                }
            }
        }, timeoutvalue));
    }
}
var variables = {
    /**
     * Initiate variables for application
     */
    init: function() {
        $paging_items = 20;
        $current_page = 0;
        $message_success_update = "<div class=\"alert alert-success alert-dismissible\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Stäng\"><span aria-hidden=\"true\">&times;</span></button><i class=\"fa fa-check-square\"></i> Informationen är sparad";
        $message_error_update = "<div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Stäng\"><span aria-hidden=\"true\">&times;</span></button>Du måste fylla i dessa fält: namn & typ";
        $message_removed = "<div class=\"alert alert-success alert-dismissible\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Stäng\"><span aria-hidden=\"true\">&times;</span></button><i class=\"fa fa-check-square\"></i> Taggen är borttagen";
        $message_doublets_links = "<div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Stäng\"><span aria-hidden=\"true\">&times;</span></button>Det finns dubletter av länkar";

        $newspilot_userdata = "";
        $newspilot_etag = "";
        $newspilot_article_id = 0;
        $newspilot_current_user = "";
        $newspilot_current_user_passw = "";
        $newspilot_vieweditdialog = false;
    }
}