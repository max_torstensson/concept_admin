var hash = {
    /**
     * Listen to hash changes from user
     */
    init: function() {
        var hashvalue = window.location.hash.replace("#",'');

        // Edit
        if(hashvalue !== undefined) {
            if (hashvalue.length > 5) {
                if (hashvalue.substring(0, 4) == "edit") {
                    editadd.edit_tag(hashvalue.substring(5));
                }
            }
        }
    }
}