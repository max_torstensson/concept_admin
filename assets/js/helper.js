var helper = {
    /**
     * Reset message text
     */
    reset_message: function()
    {
        $('#message').html("");
    },

    /**
     * Get current selected status
     */
    get_current_status: function() {
        var status = "";
        $(".status_button").each(function () {
            if ($(this).hasClass("active"))
                status += "ConceptStatus:" + $(this).data("status") + " OR ";
        });
        status = "(" + status.substring(0, status.length - 4) + ")";
        return status;
    },

    /**
     * Get current selected search sorting
     */
    get_current_sorting: function() {
        return $("#search_sorting").val();
    },

    /**
     * Get current selected type for search
     */
    get_current_type: function() {
        return $("#search_type").val();
    },

    /**
     * Set title of table
     * @param header
     * @param no_of_tags
     */
    set_title: function(header, no_of_tags) {
        if(no_of_tags / $paging_items <= 1){
            $("#tagTitle").html(header + $("#search").val() + " <small>" + no_of_tags + " taggar (Sida " + ($current_page + 1) + " av  1)</small>");
        }
        if(no_of_tags / $paging_items == 0){
            $("#tagTitle").html(header + $("#search").val() + " <small>" + no_of_tags + " taggar (Sida " + ($current_page + 1) + " av 0  )</small>");
        }
        else{
            $("#tagTitle").html(header + $("#search").val() + " <small>" + no_of_tags + " taggar (Sida " + ($current_page + 1) + " av " + Math.ceil(no_of_tags / $paging_items) + ")</small>");
        }
    },

    /**
     * Return all variables from querystring
     * @returns {Array}
     */
    getUrlVars: function()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },

    /**
     * Return _next_ status of a tag
     * @param currentstatus
     * @param returnvalue 0 = class, 1 = text, 2 = machinetext
     * @returns {string}
     */
    get_next_status: function(currentstatus, returnvalue) {
        if (currentstatus === "withheld") {
            if (returnvalue == 0)
                return "btn-success";
            else if (returnvalue == 1)
                return "Godkänd";
            else
                return "usable";
        } else if (currentstatus === "usable") {
            if (returnvalue == 0)
                return "btn-danger";
            else if (returnvalue == 1)
                return "Spärrad";
            else
                return "cancelled";
        } else if (currentstatus === "cancelled") {
            if (returnvalue == 0)
                return "btn-warning";
            else if (returnvalue == 1)
                return "Väntar";
            else
                return "withheld";
        } else
            return "";
    }
}