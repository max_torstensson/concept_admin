var editadd = {
    /**
     * Edit tag
     */
    tempData: [],
    edit_tag: function (selectedtagid) {
        helper.reset_message();
        var guid = selectedtagid;
        var currentbtn = $(this);

        // Show modal
        $("#editmodal").modal('show');

        // Change hash
        window.location.hash = "#edit/" + guid;

        // Get information about the current clicked tag
        $.get('get_data.php?action=gettag&id=' + guid, function (data) {

            // Reset all icons
            $('.btnEdit').each(function () {
                $(this).html("<i class=\"fa fa-pencil\"></i>");
                $(this).removeClass("btn-default");
                $(this).addClass("btn-primary");
            });

            // Show remove link
            $("#removetag").show();

            // Headline
            $("#formtitle").html("Redigera tagg");

            // Proposals
            var prop_name = editadd.look_for_value("ConceptProposalName", data.properties);

            var prop_username = editadd.look_for_value("ConceptProposalUser", data.properties);
            var prop_type = editadd.look_for_value("ConceptProposalType", data.properties);
            var prop_long = editadd.look_for_value("ConceptProposalDefinitionLong", data.properties);
            var prop_short = editadd.look_for_value("ConceptProposalDefinitionShort", data.properties);
            var prop_note = editadd.look_for_value("ConceptProposalNote", data.properties);
            var prop_links = editadd.look_for_value("ConceptProposalSeeAlso", data.properties);
            if (prop_name !== undefined || prop_type !== undefined || prop_long !== undefined || prop_short !== undefined || prop_note !== undefined || prop_links !== undefined) {
                $("#proposal-fields").show();

                if (prop_name !== undefined) {
                    $("#proposal_tagname").val(prop_name.values);
                }
                else
                    $("#proposal_tagname").val("");

                if (prop_type !== undefined)
                    $("#proposal_tagtype").val(prop_type.values);
                else
                    $("#proposal_tagtype").val("");

                if (prop_long !== undefined)

                    $("#proposal_description_long_admin").val(prop_long.values[0]);
                else
                    $("#proposal_description_long_admin").val("");

                if (prop_short !== undefined)
                    $("#propsal_description_short").val(prop_short.values);
                else
                    $("#propsal_description_short").val("");

                if (prop_note !== undefined)
                    $("#proposal_Note_admin").val(prop_note.values[0]);
                else
                    $("#proposal_Note_admin").val("");

                if (prop_links !== undefined) {
                    var str_links = prop_links.values.toString();
                    $("#propsal_links").html(str_links);
                }
                else
                    $("#propsal_links").html("");

                if (prop_username.values[0] !== undefined) {
                    $("#proposal_username_header").html(" Inskickat av " + prop_username.values[0] + "");
                    $("#proposal_user").val(prop_username);
                } else {
                    $("#proposal_user").val("");
                    $("#proposal_username_header").html("");
                }
            }
            // Type

            var type = editadd.look_for_value("ConceptImType", data.properties);
            if (type !== undefined) {
                $("#tagtype").val(type.values[0]);

            }
            $("#tagtype option:not(:selected)").prop("disabled", true);
            if (type.values[0] == "author") {
                $(".author").show();
                $(".name").hide();
                $('#tagname').prop('required', false);
            }
            else {
                $(".author").hide();
                $(".name").show();
                $('#tagname').prop('required', true);
            }
            if (type.values[0] == "person" || type.values[0] == "author") {
                $(".person").show();
                $(".name").hide();
                $('#tagname').prop('required', false);
            }
            else {
                $(".person").hide();
                $(".name").show();
                $('#tagname').prop('required', true);
            }
            // First created
            var firstcreated = editadd.look_for_value("created", data.properties);
            if (firstcreated !== undefined)
                $("#date_created").val(firstcreated.values[0]);

            // Status. If we have ?user=tagadmin, set status to value from OC, otherwise set status to withheld
            var status = "";
            if (helper.getUrlVars()["user"] == undefined)
                status = "withheld";
            else
                status = editadd.look_for_value("ConceptStatus", data.properties).values[0];
            if (status !== undefined)
                $("#tagstatus").val(status);

            // Note
            var Note = editadd.look_for_value("ConceptNote", data.properties);
            if (Note !== undefined)
                $("#Note").val(Note.values[0]);
            // Name
            var tagname = editadd.look_for_value("ConceptName", data.properties);
            if (tagname !== undefined) {
                $("#tagname").val(tagname.values[0]);
            }
            //firstname
            var firstname = editadd.look_for_value("ConceptFirstName", data.properties);
            if (firstname !== undefined)
                $("#firstname").val(firstname.values[0]);
            //lastname
            var lastname = editadd.look_for_value("ConceptLastName", data.properties);
            if (lastname !== undefined)
                $("#lastname").val(lastname.values[0]);
            // Tag id
            $("#tagid").val(guid);

            // Links
            form.trashFormData();
            var links = editadd.look_for_value("ConceptLinkValueAndType", data.properties);
            //vill kunna hämta en property med en en länk och en typ.
            if (links !== undefined) {

                $(".input_fields_wrap").empty();
                for (i = 0; i < links.values.length; i++) {
                    if (links.values[i] !== "::" || links.values[i] !== "") {
                        var arr = links.values[i].split("::");
                        if (arr[1] !== "") {
                            //form.populatelinks(arr[1], arr[0]);
                            editadd.tempData.push(arr[1]);
                            form.addOneLink(arr[1]);
                        }
                    }
                }

            }
            else {
                $(".input_fields_wrap").empty();
                $(".addedlinks").hide();
            }

            // Long description
            var longdesc = editadd.look_for_value("ConceptDefinitionLong", data.properties);
            if (longdesc !== undefined)
                $("#description_long").val(longdesc.values[0]);
            else
                $("#description_long").val("");

            // Short description
            var shortdesc = editadd.look_for_value("ConceptDefinitionShort", data.properties);
            if (shortdesc !== undefined)
                $("#description_short").val(shortdesc.values[0]);
            else
                $("#description_short").val("");

            // e-mail
            var email = editadd.look_for_value("ConceptEmail", data.properties);
            if (email !== undefined) {
                $("#email").val(email.values[0]);
            }
            //Phone
            var phone = editadd.look_for_value("ConceptPhone", data.properties);
            if (phone !== undefined) {
                $("#phone").val(phone.values[0]);
            }
            //streetAddress
            var streetAddress = editadd.look_for_value("ConceptStreetAddress", data.properties);
            if (streetAddress !== undefined) {
                $("#streetAddress").val(streetAddress.values[0]);
            }
            //postalcode
            var postalCode = editadd.look_for_value("ConceptPostalCode", data.properties);
            if (postalCode !== undefined) {
                $("#postalCode").val(postalCode.values[0]);
            }
            //Country
            var country = editadd.look_for_value("ConceptCountry", data.properties);
            if (country !== undefined) {
                $("#country").val(country.values[0]);
            }

        }, "json");

    },

    /**
     * Look for values in properties
     * @param key
     */
    look_for_value: function (key, properties) {
        for (var i = 0; i < properties.length; i++) {
            if (properties[i].name == key)
                return properties[i];
        }
        return undefined;
    },


    /**
     * Add tag
     */
    add_tag: function () {
        helper.reset_message();

        // Show modal
        $("#editmodal").modal('show');

        // Change hash
        window.location.hash = "#add";

        //lock upp type
        $("#tagtype option:not(:selected)").prop("disabled", false);


        // Hide propsals
        $("#proposal-fields").hide();

        // Headline
        $("#formtitle").html("Lägg till tagg");

        // Type

        $("#tagtype").val("");


        // First created
        $("#date_created").val("");

        // Status
        $("#tagstatus").val("");

        // Name
        $("#tagname").val("");

        $(".person").hide();

        //firstname
        $("#firstname").val("");
        //lastname
        $("#lastname").val("");

        // Tag id
        $("#tagid").val("");

        // Note
        $("#Note").val("");

        // Links
        $(".input_fields_wrap").empty();

        // Long description
        $("#description_long").val("");

        // Short description
        $("#description_short").val("");
        //e-mail
        $("#email").val("");
        //phone
        $("#phone").val("");
        //Street adress
        $("#streetAddress").val("");
        //Postal code
        $("#postalCode").val("");
        //country
        $("#country").val("");
    },

    /**
     * Event handling for edit and add function
     */
    edit_event: function () {
        $('body').on('click', '.btnEdit', function () {
            editadd.edit_tag($(this).data("id"));

        });

        $('#editmodal').on('hidden.bs.modal', function (e) {
            editadd.tempData = [];
            $('.link').val("");
            $('#ErrorMessagePopUp').hide();
        });

        $('body').on('click', '#createtag', function () {

            $(".author").hide();
            $(".addedlinks").hide();
            editadd.add_tag();
        });
        $('body').on('click', '.showedit', function () {
            editadd.show_edit_tag($(this).data("tagid"));
        });
    },


    /**
     * Shoew edit tag information and proposals in newspilot
     */
    show_edit_tag: function (selectedtagid) {
        helper.reset_message();
        var guid = selectedtagid;
        var currentbtn = $(this);

        // Show modal
        $("#showedit").modal('show');

        // Get information about the current clicked tag
        $.get('get_data.php?action=gettag&id=' + guid, function (data) {


            // Clean up proposal information
            $("#proposal_description_long").val("");
            $("#proposal_description_short").val("");
            $("#proposal_Note").val("");
            //Mail

            //username

            // Type
            var type = editadd.look_for_value("ConceptType", data.properties);
            if (type !== undefined) {
                $("#show_tagtype").val(type.values[0]);
                $("#proposal_tagtype").val(type.values[0]);

            }

            // First created
            var firstcreated = editadd.look_for_value("created", data.properties);
            if (firstcreated !== undefined)
                $("#show_date_created").val(firstcreated.values[0]);

            // Status. If we have ?user=tagadmin, set status to value from OC, otherwise set status to withheld
            var status = "";
            if (helper.getUrlVars()["user"] == undefined)
                status = "withheld";
            else
                status = editadd.look_for_value("ConceptStatus", data.properties).values[0];
            if (status !== undefined)
                $("#show_tagstatus").val(status);

            // Note
            var Note = editadd.look_for_value("ConceptNote", data.properties);
            if (Note !== undefined) {
                $("#show_Note_info").val(Note.values[0]);
                $("#show_Note").val(Note.values[0]);
            }

            // Name
            var tagname = editadd.look_for_value("ConceptName", data.properties);
            if (tagname !== undefined) {
                $("#show_tagname").val(tagname.values[0]);
                $("#show_tagname_info").val(tagname.values[0]);

            }

            // Tag id
            $("#show_tagid").val(guid);

            // Links
            $(".input_fields_wrap").empty();
            var links = editadd.look_for_value("ConceptLinkValueAndType", data.properties);
            for (i = 0; i < links.length; i++) {
                console.log(links[i]);
            }

            var str_links = "";
            if (links !== undefined)
                str_links = links.values.toString();
            $("#show_links").html(str_links);

            // Long description
            var longdesc = editadd.look_for_value("ConceptDefinitionLong", data.properties);
            if (longdesc !== undefined) {
                $("#show_description_long_info").val(longdesc.values[0]);
                $("#show_description_long").val(longdesc.values[0]);
            }
            else {
                $("#show_description_long_info").val("");
                $("#show_description_long").val("");
            }

            // Short description
            var shortdesc = editadd.look_for_value("ConceptDefinitionShort", data.properties);
            if (shortdesc !== undefined) {
                $("#show_description_short_info").val(shortdesc.values[0]);
                $("#show_description_short").val(shortdesc.values[0]);
            }
            else {
                $("#show_description_short_info").val("");
                $("#show_description_short").val("");
            }
        }, "json");
    },

    /**
     * Change status of tag
     */
    change_tag_status: function () {
        $('body').on('click', '.btnStatus', function () {
            var id = $(this).data("id");
            var currentstatus = $(this).data("currentstatus");
            var currentbtn = $(this);

            // Update data in API
            $.get('update_data.php?action=updatestatus&id=' + id + "&current=" + currentstatus, function (data) {
                // Change status of buttons
                $("#status_" + id).removeClass("btn-warning").removeClass("btn-danger").removeClass("btn-success");
                $("#status_" + id).addClass(helper.get_next_status(currentstatus, 0));
                $("#status_" + id).html(helper.get_next_status(currentstatus, 1));
                currentbtn.data("currentstatus", helper.get_next_status(currentstatus, 2));
                search.search_tags(500);
            });
        });
    }
}