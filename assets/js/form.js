var form = {
    /**
     * Submit form with ajax
     */

    formData: [],
    RemovedsLinks: [],
    submitform: function () {

        var self = this;
        console.log(self.formData);
        if (!form.checkform()) {
            $('#ErrorMessagePopUp').html($message_error_update);
            $("#loginiconform").hide();
            $("#tagname").focus();
            return false;
        }
        var tagform = $("#tagform"),
            tagid = $("#tagid").val(),
            data = tagform.find(':input:not([name="links"])').serialize();
        var form_action = tagid == "" ? "add_data.php" : "update_data.php?action=updatetag&id=" + tagid;
        $("#loginiconform").show();
        console.log(data);
        $.ajax({
            type: "POST",
            url: form_action,
            data: {data: data, formData: self.formData, removedlinks: self.RemovedsLinks},
            success: function () {
                window.clearTimeout($(this).data("timeout"));
                $(this).data("timeout", setTimeout(function () {
                    if (typeof $newspilot !== 'undefined') {
                        // Get information about the recent added tag
                        $.get('get_data.php?action=gettag_by_name&search=' + $("#tagname").val() + "&start=0&limit=1&type=all&sorting=name", function (data) {
                            newspilot.add_tag(data.id, data.versions[0].properties.ConceptName[0], data.versions[0].properties.ConceptStatus[0]);
                            $("#editmodal").modal('hide');
                        }, "json");
                    } else {
                        this.search.search_tags(500);
                        $("#editmodal").modal('hide');
                        $('#message').html($message_success_update);
                        $("#loginiconform").hide();
                    }
                }, 1000));
            }
        });
        self.formData = [];
        return false;
    },

    /**
     * Submit showedit newspilot form
     */
    showedit_submit: function () {
        var tagform = $("#show_tagform"),
            tagid = $("#show_tagid").val(),
            formData = [],
            data = tagform.find(':input:not([name="links"])').serialize();
        tagform.find('input[name="links"]').each(function () {
            formData.push(this.value);
        });
        data += '&links=' + formData.join(',');

        var form_action = "update_data.php?action=updateproposal&id=" + tagid + "&user=" + variables.$newspilot_current_user;
        $("#loginiconform").show();
        $.ajax({
            type: "POST",
            url: form_action,
            data: data,
            success: function () {
                window.clearTimeout($(this).data("timeout"));
                $(this).data("timeout", setTimeout(function () {
                    $("#showedit").modal('hide');
                    $("#loginiconform").hide();
                }, 1000));
            }
        });
        return false;
    },

    /**
     * Check form values
     */
    checkform: function () {
        /*
         if ($("#tagname").val() === "")
         return false;
         */
        if ($("#tagtype").val() === "")
            return false;
        return true;
    },

    /**
     * Click to show more links
     */
    populatelinks: function ($link, $type) {
        var self = this;
        self.formData.push({link: $link, type: $type})


    },
    trashFormData: function () {
        var self = this;
        self.formData = [];
    },

    addlinks: function () {
        var wrapper = $(".input_fields_wrap");
        var linkform = $(".add_links_form");
        //var add_button = $(".add_field_button");
        var self = this;
        //on add input button click
        $(linkform).append('<div class="tag-row">' +
            '<div class="form-inline">' +
            '<input type="text" class="form-control link" name="links">' +
            '<select class="selectpicker form-control" id="seltypelink" title="Please select a type">' +
            '<option value="x-im/social+facebook">Facebook</option> ' +
            '<option value="x-im/social+twitter">Twitter</option> + ' +
            '<option>Gravatar</option> ' +
            '<option value="x-np/user">Newspilot</option> ' +
            '<option value="x-ad/sam_account_name">AD</option> ' +
            '<option value="text/html">Övrigt</option> ' +
            '</select>' +
            '<a class="btn btn-success" id="addlink"><i class="fa fa-plus"></i></a>' +
            '</div>');
        $(wrapper).on("click", ".remove_field", function (e) {
            e.preventDefault();
            $value = $(this).siblings().val();
            var index = editadd.tempData.indexOf($value);
            editadd.tempData.splice(index, 1);
            //remove from array
            self.RemovedsLinks.push($value);
            form.RemoveFormDataItem($value);
            $(this).parent('div').remove();
            $linksitem = $('.links').length;
            if ($linksitem == 0) {
                $('.addedlinks').hide();
            }
            var bool = form.identifyDuplicatesFromArray(editadd.tempData);
            if (bool == false) {
                helper.reset_message();
                $("#ErrorMessagePopUp").hide();
            }
        });
        $(linkform).on("click", "#addlink", function (e) {
            var link = $('.link').val();
            var bool = form.isExist(editadd.tempData, link);
            if (bool == true) {
                $('#ErrorMessagePopUp').show();
                $('#ErrorMessagePopUp').html($message_doublets_links);
            }
            if (bool !== true) {
                editadd.tempData.push(link);
                var seltyp = $('#seltypelink :selected').val();
                form.populatelinks(link, seltyp);
                $('.link').val("");
                form.addOneLink(link);
            }
            // console.log(self.formData);
        });
        $("#addbtn").click(function () {
            $(".showlinks").toggle();
        });
    },
    isExist: function (arr, str) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === str) {
                // Return index
                return true;
            } else {
                /* If value is not found return -1. */
                return false;
            }
        }
    },
    identifyDuplicatesFromArray: function (arr) {
        var i;
        var len = arr.length;
        var obj = {};
        var duplicates = [];
        for (i = 0; i < len; i++) {
            if (!obj[arr[i]]) {
                obj[arr[i]] = {};
            }
            else {
                return true
            }
        }
        return false;
    },
    RemoveFormDataItem: function (itemName) {
        var self = this;
        for (var i = 0; i < self.formData.length; i++) {
            if (self.formData[i].link == itemName) {
                self.formData.splice(i, 1);
                break;
            }
        }
    },
    /**
     * Add one link from edit form
     * @param url
     */
    addOneLink: function (url) {

        $(".input_fields_wrap").append('' +
            '<div class="tag-row">' +
            '<div class="form-inline">' +
            '<input type="text" class="form-control links" name="links" value="' + url + '" disabled>' +
            ' <a href="#" class="btn btn-default remove_field"><i class="fa fa-trash"></i></a>' +
            '</div></div>');

        //add label
        $('.addedlinks').show();
    },

    /**
     * Empty poposals fields
     */
    empty_proposals: function () {
        $("#proposal_tagname").val("");
        $("#proposal_tagtype").val("");
        s
        $("#proposal_description_long_admin").val("");
        $("#propsal_description_short").val("");
        $("#proposal_Note_admin").val("");
        $("#propsal_links").html("");
    }
}