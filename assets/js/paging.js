var paging = {
    /**
     * Listen for paging events
     */
    paging_event: function () {
        $('body').on('click', '.pagingbutton', function () {
            paging.change_paging($(this).data('page'));
        });
    },

    /**
     * Change current page, make a new search and scroll up
     * @param page
     */
    change_paging: function (page) {
        $current_page = page;
        if (page <= -1 || page == null) {
            return;
        }
        search.search_tags();
        $("html, body").animate({scrollTop: 0}, "fast");

        // Change hash
        window.location.hash = "#page/" + page;
    },

    /**
     * Print paging form on the page
     */
    print_paging: function (total) {
        var pages = (total / $paging_items);
        var pagingcontent = "";
        if ($current_page > pages) {
            $current_page = 0;
            search.search_tags();
            paging.change_paging(0);
        }
        console.log($current_page);
        if ($current_page >= 1) {
            pagingcontent += "<button type=\"button\" data-page=\"" + 0 + "\" class=\"btn btn-default pagingbutton\"><i class='fa fa-fast-backward' aria-hidden='true'></i></button>";
            pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page - 1) + "\" class=\"btn btn-default btn-grey pagingbutton \"><i class='fa fa-step-backward' aria-hidden='true'></i></button>";
        }

        // Pages before current_page
        // Page buttons

        if ($current_page == 1) {
            for (i = 1; --i >= 0;) {
                pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page - i - 1) + "\" class=\"btn btn-default pagingbutton\">" + ($current_page - i) + "</button>";
            }
        }
        if ($current_page > 1) {
            for (i = 2; --i >= 0;) {
                pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page - i - 1) + "\" class=\"btn btn-default pagingbutton\">" + ($current_page - i) + "</button>";
            }
        }
        // Pages behind current_page
        // Current page
        pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page) + "\" class=\"btn btn-primary pagingbutton\">" + ($current_page + 1) + "</button>";

        for (i = 1; i < 3; i++) {
            if (($current_page + i) <= pages)
                pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page + i) + "\" class=\"btn btn-default pagingbutton\">" + ($current_page + i + 1) + "</button>";
        }


        // Pages before current_page
        if (pages > 1 && $current_page > 0 && pages.toFixed(0) > $current_page) {


            // Forward button

            pagingcontent += "<button type=\"button\" data-page=\"" + ($current_page + 1) + "\" class=\"btn btn-default pagingbutton\"> <i class=\"fa fa-step-forward\"></i></button>";
        }

        // Set content
        $("#paging").html(pagingcontent);
    }
};