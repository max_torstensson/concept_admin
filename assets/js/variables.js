var variables = {
    /**
     * Initiate variables for application
     */
    init: function() {
        $paging_items = 20;
        $current_page = 0;
        $message_success_update = "<div class=\"alert alert-success alert-dismissible\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Stäng\"><span aria-hidden=\"true\">&times;</span></button><i class=\"fa fa-check-square\"></i> Informationen är sparad";
        $message_error_update = "<div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Stäng\"><span aria-hidden=\"true\">&times;</span></button>Du måste fylla i dessa fält: namn & typ";
        $message_removed = "<div class=\"alert alert-success alert-dismissible\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Stäng\"><span aria-hidden=\"true\">&times;</span></button><i class=\"fa fa-check-square\"></i> Taggen är borttagen";
        $message_doublets_links = "<div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Stäng\"><span aria-hidden=\"true\">&times;</span></button>Det finns dubletter av länkar";

        $newspilot_userdata = "";
        $newspilot_etag = "";
        $newspilot_article_id = 0;
        $newspilot_current_user = "";
        $newspilot_current_user_passw = "";
        $newspilot_vieweditdialog = false;
    }
}