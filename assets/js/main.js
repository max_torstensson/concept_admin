// https://incident57.com/codekit/help.html#javascript
// @codekit-prepend "variables.js";
// @codekit-prepend "edit.js";
// @codekit-prepend "search.js";
// @codekit-prepend "paging.js";
// @codekit-prepend "form.js";
// @codekit-prepend "helper.js";
// @codekit-prepend "delete.js";
// @codekit-prepend "hash.js";
// @codekit-prepend "newspilot.js";
// @codekit-prepend "base64.js";

$(function () {
    // Init variables
    variables.init();

    // Focus search box on start and search for tags when a user type in a search textbox
    if (document.getElementById("search") !== null) {
        $("#search").focus().bind("input propertychange", function (evt) {
            if (window.event && event.type == "propertychange" && event.propertyName != "value")
                return;
            search.search_tags();
        });
    }
    $(".btn").mouseup(function(){
        $(this).blur();
    })
    // Events for edit, delete tag and change tag status
    editadd.edit_event();
    delete_tag.del();
    editadd.change_tag_status();
    //
    $("#search").keypress(function(e){
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            return false;
        }
    });

    // Click on status buttons
    $(".status_button").click(function () {
        search.search_tags();
    });

    // Change sorting selectbox
    $("#search_sorting").change(function () {
        search.search_tags();
    });

    // Change type selectbox
    $("#search_type").change(function () {
        search.search_tags();
    });
    var type = $("#tagtype").val();
    if (type == "author") {
        $(".author").show();
    }
    else {
        $(".author").hide();
    }
    if (type == "person" || type == "author") {
        $(".person").show();
        $(".name").hide();
        $('#tagname').attr("required", false);
    }
    else {
        $(".person").hide();
        $(".name").show();
        $('#tagname').attr("required", true);
    }
    $("#tagtype").change(function (e) {
        var type = $("#tagtype").val();
        if (type == "author") {
            $(".author").show();
        }
        else {
            $(".author").hide();
        }
        if (type == "person" || type == "author") {
            $(".person").show();
            $(".name").hide();
            $('#tagname').prop('required',false);
        }
        else {
            $(".person").hide();
            $(".name").show();
            $('#tagname').prop('required',true);
        }
    });
    // Submit form
    $(".submitbutton").click(function (e) {
        e.preventDefault();
        form.submitform();


    });
    $("#tagform").submit(function (e) {
        e.preventDefault();
        form.submitform();

    });


    // Submit form view/edit in newspilot
    $(".submitviewbutton").click(function (e) {
        e.preventDefault();
        form.showedit_submit();
    });
    $("#show_tagform").submit(function (e) {
        e.preventDefault();
        form.showedit_submit();
    });

    // Verification mode for all search filters
    $("#verifymode").click(function () {
        $("#search_sorting").val("date");
        $("#search_type").val("all");
        $("#status_withheld").addClass("active");
        $("#status_usable").removeClass("active");
        $("#status_cancelled").removeClass("active");
        search.search_tags();
    });

    // Button for empty proposals
    $('body').on('click', '.emptyproposals', function () {
        form.empty_proposals();
    });

    if (document.getElementById("search") !== null) {
        // Paging events
        paging.paging_event();

        // Initial search
        search.search_tags();
    }

    // Event handler for hash changes
    window.onload = function () {
        hash.init();
    };

    // Event handler for more link-buttons
    form.addlinks();

    // Events for newspilotpage
    if (document.getElementById("search_newspilot") !== null)
        newspilot.init();
});