var delete_tag = {
    /**
     * Delete tag from API
     */
    del: function()
    {
        $('body').on('click', '#removetag', function (e) {
            var id = $("#tagid").val();
            e.preventDefault();
            if (confirm("Vill du verkligen ta bort taggen?")) {
                $.get('update_data.php?action=deletetag&id=' + id, function (data) {
                    window.clearTimeout($(this).data("timeout"));
                    $(this).data("timeout", setTimeout(function () {
                        search.search_tags(500);
                        $('#message').html($message_removed);
                        $("#editmodal").modal('hide');
                    }, 1000));
                });
            }
        });
    }
}