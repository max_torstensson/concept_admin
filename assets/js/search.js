var search = {

    /**
     * Search function for tags
     */
    search_tags: function(timeoutvalue)
    {

        timeoutvalue = typeof timeoutvalue !== 'undefined' ? timeoutvalue : 200;
        window.clearTimeout($(this).data("timeout"));
        $(this).data("timeout", setTimeout(function () {
            $("#tagTable thead").show();
            var searchbox = $("#search").val();
            if (searchbox != "") {
                if (searchbox.length > 1) {
                    $("#loadingicon").show();
                    $.get('get_data.php?search=' + searchbox + "&start=" + $current_page + "&ConceptStatus=" + helper.get_current_status() + "&limit=" + $paging_items + "&sorting=" + helper.get_current_sorting() + "&type=" + helper.get_current_type(), function (data) {
                        $("#loadingicon").hide();
                        helper.set_title("Sökresultat: ", data[0]);
                        if (data[1] != "")
                            $("#tagTable tbody").html(data[1]);
                        else {
                            $("#tagTable thead").hide();
                            $("#tagTable tbody").html("<tr><td colspan='4'>Din sökning på <b>" + searchbox + "</b> returnerade inget resultat.</td></tr>");
                        }
                        paging.print_paging(data[0]);
                    }, "json");
                }
            } else {
                $.get('get_data.php?ConceptStatus=' + helper.get_current_status() + "&start=" + $current_page + "&limit=" + $paging_items + "&sorting=" + helper.get_current_sorting() + "&type=" + helper.get_current_type(), function (data) {
                    helper.set_title("Senaste skapade taggar ", data[0]);
                    $("#tagTable tbody").html(data[1]);
                    paging.print_paging(data[0]);
                }, "json");
            }
        }, timeoutvalue));
    },

    /**
     * Search for one tag for newspilot integration
     */
    search_tag_newspilot: function(timeoutvalue) {
        timeoutvalue = typeof timeoutvalue !== 'undefined' ? timeoutvalue : 200;
        window.clearTimeout($(this).data("timeout"));
        $(this).data("timeout", setTimeout(function () {
            var searchbox = $("#search_newspilot").val();
            if (searchbox != "") {
                if (searchbox.length > 1) {
                    $("#search_newspilot_result").hide();
                    $("#search_in_progress").show();
                    $.get('get_data_newspilot.php?function=tag&search=' + searchbox +'&type=all&limit=70&start=0&sorting=&user=' + variables.$newspilot_current_user +'&passw=' + variables.$newspilot_current_user_passw, function (data) {
                        $("#search_in_progress").hide();
                        $("#search_newspilot_result").show();
                        if (data != "")
                            $("#search_newspilot_result").html(data);
                        else {
                            $("#search_newspilot_result").html("<div class=\"infobox\"><div class=\"alert alert-info\" role=\"alert\">Taggen <b>" + searchbox + "</b> finns inte.<br><a id=\"createtag\" class=\"alert-link\">Vill du lägga till den?</a></div></div>");
                        }
                    }, "json");
                }
            }
        }, timeoutvalue));
    },


    search_story_newspilot: function(timeoutvalue) {
        timeoutvalue = typeof timeoutvalue !== 'undefined' ? timeoutvalue : 200;
        window.clearTimeout($(this).data("timeout"));
        $(this).data("timeout", setTimeout(function () {
            var searchbox = $("#search_newspilot_story").val();
            if (searchbox != "") {
                if (searchbox.length > 1) {
                    $("#search_newspilot_result_stories").hide();
                    $("#search_in_progress_story").show();
                    $.get('get_data_newspilot.php?function=story&search=' + searchbox +'&type=all&limit=70&start=0&sorting=&user=' + variables.$newspilot_current_user +'&passw=' + variables.$newspilot_current_user_passw, function (data) {
                        $("#search_in_progress_story").hide();
                        $("#search_newspilot_result_stories").show();
                        if (data != "")
                            $("#search_newspilot_result_stories").html(data);
                        else {
                            $("#search_newspilot_result_stories").html("<div class=\"infobox\"><div class=\"alert alert-info\" role=\"alert\">Storyn <b>" + searchbox + "</b> finns inte.<br><a id=\"createtag\" class=\"alert-link\">Vill du lägga till den?</a></div></div>");
                        }
                    }, "json");
                }
            }
        }, timeoutvalue));
    }
}