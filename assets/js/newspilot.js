var newspilot = {
    /**
     * Initiate search function and event handling for buttons
     */
    init: function() {
        // Focus search box on newspilot page and search for tags when a user type in a search textbox
        $("#search_newspilot").focus().bind("input propertychange", function (evt) {
            if (window.event && event.type == "propertychange" && event.propertyName != "value")
                return;
            search.search_tag_newspilot();
        });

        // Focus story search box on newspilot page and search for tags when a user type in a search textbox
        $("#search_newspilot_story").focus().bind("input propertychange", function (evt) {
            if (window.event && event.type == "propertychange" && event.propertyName != "value")
                return;
            search.search_story_newspilot();
        });

        // Check if view/edit-dialog is shown
        $('#showedit').on('show.bs.modal', function (e) {
            variables.$newspilot_vieweditdialog = "true";
        });
        $('#showedit').on('hide.bs.modal', function (e) {
            variables.$newspilot_vieweditdialog = "false";
        });

        // Add tag to collection
        $('body').on('click', '.addtocollection', function () {
            newspilot.add_tag($(this).data('tagid'), $(this).data('tagname'), $(this).data('tagstatus'));
        });

        // Remove tag from collection
        $('body').on('click', '.removefromcollection', function () {
            newspilot.delete_tag($(this).data('tagid'));
        });

        // Add story to collection
        $('body').on('click', '.addtocollection_story', function () {
            newspilot.add_story($(this).data('tagid'), $(this).data('tagname'), $(this).data('tagstatus'));
        });

        // Remove tag from collection
        $('body').on('click', '.removefromcollection_story', function () {
            newspilot.delete_story($(this).data('tagid'));
        });

        // Get values from newspilot
        variables.$newspilot_article_id = getNewspilotArticleIdAsString();
        variables.$newspilot_current_user = GetNewspilotLogin();
        variables.$newspilot_current_user_passw = GetNewspilotPassword();

        if(variables.$newspilot_article_id === "" || variables.$newspilot_article_id === undefined) {
            $('#debug').html("<b>Fel på koppling till newspilot. Kontakta IT-support.</b>")
        } else {
            // Get tags and stories from from selected article
            $.get('get_data_newspilot.php?function=articletags&articleid=' + variables.$newspilot_article_id +'&user=' + variables.$newspilot_current_user +'&passw=' + variables.$newspilot_current_user_passw, function (data) {
                // data[0] = tags, data[1] = original userdata, data[2] = etag, data[3] = articleId, data[4] = stories

                if($data[0] != null) {
                    $("#notags").remove();
                    for (var i = 0; i < data[0].length; i++) {
                        $("#newspilot_collection").prepend("<tr class=\"tag_in_collection\" data-tagid=\"" + data[0][i].id + "\"><td><button class=\"btn " + newspilot.get_tagstatus_class(data[0][i].versions[0].properties.ConceptStatus[0]) + " btn-circle btn-xs\" type=\"button\"> </button> " + data[0][i].versions[0].properties.ConceptName[0] + "</td><td><a class=\"removefromcollection\"data-tagid=\"" + data[0][i].id + "\"><i class=\"fa fa-trash\"></i></a></td></tr>");
                    }
                }
                if($data[4] != null) {
                    $("#nostories").remove();
                    for (var i = 0; i < data[4].length; i++) {
                        $("#newspilot_stories_collection").prepend("<tr class=\"story_in_collection\" data-tagid=\"" + data[4][i].id + "\"><td><button class=\"btn " + newspilot.get_tagstatus_class(data[4][i].versions[0].properties.ConceptStatus[0]) + " btn-circle btn-xs\" type=\"button\"> </button> " + data[4][i].versions[0].properties.ConceptName[0] + "</td><td><a class=\"removefromcollection_story\"data-tagid=\"" + data[4][i].id + "\"><i class=\"fa fa-trash\"></i></a></td></tr>");
                    }
                }
                variables.$newspilot_userdata = data[1];
                variables.$newspilot_etag = data[2];
                variables.$newspilot_article_id = data[3];

            }, "json");
        }
    },

    /**
     * Edit user data on article
     * @returns {boolean}
     */
    edit_userdata: function() {
        // String with uuid:s of articletags
        var tag_uuids = "";
        $(".tag_in_collection").each(function () {
            tag_uuids += $(this).data("tagid") +",";
        });
        tag_uuids = tag_uuids.slice(0, -1);

        // String with uuid:s of stories
        var story_uuids = "";
        $(".story_in_collection").each(function () {
            story_uuids += $(this).data("tagid") +",";
        });
        story_uuids = story_uuids.slice(0, -1);

        data = "etag=" +variables.$newspilot_etag +"&userdata=" +variables.$newspilot_userdata +"&tags=" +tag_uuids +"&stories=" +story_uuids;

        // Post information to backend
        $.ajax({
            type: "POST",
            url: "update_data.php?action=updateUserData&id=" +variables.$newspilot_article_id  +'&user=' + variables.$newspilot_current_user +'&passw=' + variables.$newspilot_current_user_passw,
            data: data,
            success: function (data) {
                return true;
            }
        });
        return false;
    },

    /**
     * Add tag to collection
     */
    add_tag: function(id, name, status) {
        if(!newspilot.tag_in_collection(id)) {
            $("#notags").remove();
            $("#newspilot_collection").prepend("<tr class=\"tag_in_collection\" data-tagid=\"" + id + "\"><td><button class=\"btn " + newspilot.get_tagstatus_class(status)  +" btn-circle btn-xs\" type=\"button\"> </button> " + name + "</td><td><a class=\"removefromcollection\"data-tagid=\"" + id + "\"><i class=\"fa fa-trash\"></i></a></td></tr>");
        }
    },

    /**
     * Delete tag from collection
     */
    delete_tag: function(id) {
        $(".tag_in_collection").each(function () {
            if (String($(this).data("tagid")) == String(id))
                $(this).remove();
        });
    },

    /**
     * Add story to collection
     */
    add_story: function(id, name, status) {
        if(!newspilot.story_in_collection(id)) {
            $("#nostories").remove();
            $("#newspilot_stories_collection").prepend("<tr class=\"story_in_collection\" data-tagid=\"" + id + "\"><td><button class=\"btn " + newspilot.get_tagstatus_class(status)  +" btn-circle btn-xs\" type=\"button\"> </button> " + name + "</td><td><a class=\"removefromcollection_story\" data-tagid=\"" + id + "\"><i class=\"fa fa-trash\"></i></a></td></tr>");
        }
    },

    /**
     * Delete story from collection
     */
    delete_story: function(id) {
        $(".story_in_collection").each(function () {
            if (String($(this).data("tagid")) == String(id))
                $(this).remove();
        });
    },

    /**
     * Check if a tag is in collection
     * @param id
     */
    tag_in_collection: function(id) {
        $(".tag_in_collection").each(function () {
            if (String($(this).data("tagid")) == String(id))
                return true;
        });
        return false;
    },

    /**
     * Check if a story is in collection
     * @param id
     */
    story_in_collection: function(id) {
        $(".story_in_collection").each(function () {
            if (String($(this).data("tagid")) == String(id))
                return true;
        });
        return false;
    },

    /***
     * Return button class for status
     * @param status
     * @returns {string}
     */
    get_tagstatus_class: function(status) {
        if(status == "withheld")
            return "btn-warning";
        else if(status == "cancelled")
            return "btn-danger";
        else if(status == "usable")
            return "btn-success";
        else
            return "";
    }
};

/*
 Special functions for newspilotintegration, documentation: https://wiki.infomaker.se/display/NPM42SVE/Webbvy-funktioner https://wiki.infomaker.se/display/NPM42SVE/Fliken+Webbvyer+4.7
 */
function getOutputResult() {
    //if(variables.$newspilot_vieweditdialog == "false")
        return newspilot.edit_userdata();
    //else
    //    return "false";
}
function getNewspilotArticleIdAsString() {
    var selection = GetNewspilotSelection();

    var jsonstr = Base64.decode(selection);
    var arrayOfObjects = eval(jsonstr);
    var arrayAsString = "";


    for (var i = 0; i < arrayOfObjects.length; i++) {
        var object = arrayOfObjects[i];
        for (var property in object) {
            if(property == "id")
                arrayAsString += object[property];
        }
    }

    return arrayAsString;
}