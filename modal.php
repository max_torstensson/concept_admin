<div class="modal fade inmodal" id="editmodal">
    <div class="modal-dialog">
        <div class="modal-content animated fadeInDown">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Stäng"><span
                        aria-hidden="true">&times;</span></button>
                <h4 id="formtitle">Skapa ny tagg</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="" id="tagform">
                    <input type="hidden" name="status" id="tagstatus" value="">
                    <input type="hidden" name="date_created" id="date_created" value="">
                    <input type="hidden" name="id" id="tagid" value="">
                    <fieldset>

                        <table class="table table-condensed" style="border-top:none;">
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <span class="label label-primary tableheader">Originalinfo</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label">Typ <span
                                                class="required">*</span></label>
                                        <select class="form-control" required="true" name="type" id="tagtype">
                                            <option value="">Välj en typ</option>
                                            <option value="author">Author</option>
                                            <option value="content-profile">Funktionstagg</option>
                                            <option value="category">Kategori</option>
                                            <option value="organisation">Organisation</option>
                                            <option value="person">Person</option>
                                            <option value="place">Plats</option>
                                            <option value="story">Story</option>
                                            <option value="topic">Ämne</option>
                                        </select>
                                    </div>
                                    <!-- person -->
                                    <div class="person">
                                        <fieldset class="form-group col-xs-12 col-sm-6">
                                            <label for="name" class="control-label">Förnamn</label>
                                            <input class="form-control" name="firstname" type="text" id="firstname">
                                        </fieldset>
                                        <fieldset class="form-group col-xs-12 col-sm-6">
                                            <label for="name" class="control-label">Efternamn</label>
                                            <input class="form-control" name="lastname" type="text" id="lastname">
                                        </fieldset>
                                    </div>
                                    <!-- name -->
                                    <div class="form-group name">
                                        <label for="name" class="control-label">Namn <span
                                                class="required">*</span></label>
                                        <input class="form-control" name="name" required="false" type="text"
                                               id="tagname">
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label">Kort beskrivning</label>
                                        <input class="form-control" name="description_short" type="text"
                                               id="description_short">
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label">Lång beskrivning</label>
                                        <textarea class="form-control" rows="4" name="description_long"
                                                  id="description_long"></textarea>
                                    </div>

                                    <!--
                                  <div class="form-group">
                                      <label for="note" class="control-label">Notering</label>
                                      <textarea class="form-control" rows="3" name="Note" id="Note"></textarea>
                                  </div>
                                  -->
                                    <!--Author -->
                                    <div class="form-group author" style="display: none;">
                                        <label for="name" class="control-label">E-mail</label>
                                        <input class="form-control" name="email" type="text" id="email">
                                    </div>
                                    <div class="form-group author" style="display: none;">
                                        <label for="name" class="control-label">Telefon</label>
                                        <input class="form-control" name="phone" type="text" id="phone">
                                    </div>
                                    <div class="form-group author" style="display: none;">
                                        <label for="name" class="control-label">Adress</label>
                                        <input class="form-control" name="streetAddress" type="text" id="streetAddress">
                                    </div>
                                    <div class="form-group author" style="display: none;">
                                        <label for="name" class="control-label">Postnummer</label>
                                        <input class="form-control" name="postalCode" type="text" id="postalCode">
                                    </div>
                                    <div class="form-group author" style="display: none;">
                                        <label for="name" class="control-label">Land</label>
                                        <input class="form-control" name="country" type="text" id="country">
                                    </div>
                                    <!-- //Author -->
                                    <div class="form-group">
                                        <label for="name" class="control-label">Länkar</label>
                                        <div class="showlinks">
                                            <div class="add_links_form">

                                            </div>
                                            <label for="addedlinks" class="control-label addedlinks"
                                                   style="display: none;">Lista på länkar</label>
                                            <div class="input_fields_wrap">
                                                <div class="tag-row">
                                                    <div class="form-inline">
                                                        <input type="text" class="form-control" name="links"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <!--
                            <td id="proposal-fields">
                                <div class="form-group">
                                  <span class="label label-primary tableheader">Förslag</span> <span id="proposal_username_header"></span>
                                  <input type="hidden" name="proposal_user" id="proposal_user" value="">
                                </div>
                              <div class="form-group">
                                <label for="name" class="control-label">Namn</label>
                                <input class="form-control" name="proposal_name" type="text" id="proposal_tagname">
                              </div>
                              <div class="form-group">
                                <label for="name" class="control-label">Typ</label>
                                <input class="form-control" name="proposal_tagtype" type="text" id="proposal_tagtype">
                              </div>
                                <div class="form-group">
                                  <label for="name" class="control-label">Lång beskrivning</label>
                                  <textarea class="form-control" rows="4" name="proposal_long_description" id="proposal_description_long_admin"></textarea>
                                </div>
                                <div class="form-group">
                                  <label for="name" class="control-label">Kort beskrivning</label>
                                  <input class="form-control" name="proposal_short_description" type="text" id="propsal_description_short">
                                </div>
                                <div class="form-group">
                                  <label for="note" class="control-label">Notering</label>
                                  <textarea class="form-control" rows="3" name="proposal_note" id="proposal_Note_admin"></textarea>
                                </div>
                                <div class="form-group">
                                  <label for="name" class="control-label">Länkar</label>
                                  <div id="propsal_links"></div>
                                </div>
                            </td>-->
                            </tr>
                        </table>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <span class="pull-left">
                    <a href="" id="removetag" title="Ta bort taggen">Ta bort tagg</a>
                </span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Stäng</button>
                <button type="submit" class="submitbutton btn btn-primary">Spara</button>
                <span><img src="assets/images/ajax-loader.gif" alt="Laddar" id="loginiconform"></span>
                <div id="ErrorMessagePopUp"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade inmodal" id="showedit">
    <div class="modal-dialog">
        <form method="POST" action="" id="show_tagform">
            <div class="modal-content animated fadeInDown">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Stäng"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 id="formtitle">Visa information</h4>
                    <p>Här kan du se mer information om taggen samt skicka in förslag på ändringar.</p>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="status" id="show_tagstatus" value="">
                    <input type="hidden" name="date_created" id="show_date_created" value="">
                    <input type="hidden" name="id" id="show_tagid" value="">
                    <table class="table table-condensed">
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label class="control-label">Originalinformation</label>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">Typ</label>
                                    <select class="form-control" required="true" name="type" id="tagtype" disabled>
                                        <option value="">Välj en typ</option>
                                        <option value="author">Author</option>
                                        <option value="content-profile">Funktionstagg</option>
                                        <option value="category">Kategori</option>
                                        <option value="organisation">Organisation</option>
                                        <option value="place">Plats</option>
                                        <option value="story">Story</option>
                                        <option value="topic">Ämne</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">Namn <span class="required">*</span></label>
                                    <input class="form-control" disabled name="name_original" required="true"
                                           type="text" id="show_tagname">
                                    <input type="hidden" name="name_original" id="show_tagname_info" value="">
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">Kort beskrivning</label>
                                    <input class="form-control" disabled name="description_short" type="text"
                                           id="show_description_short">
                                    <input type="hidden" name="description_short_original"
                                           id="show_description_short_info" value="">
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">Lång beskrivning</label>
                                    <textarea class="form-control" disabled rows="4" name="description_long"
                                              id="show_description_long"></textarea>
                                    <input type="hidden" name="description_long_original"
                                           id="show_description_long_info" value="">
                                </div>
                                <div class="form-group">
                                    <label for="note" class="control-label">Notering</label>
                                    <textarea class="form-control" disabled rows="2" name="Note"
                                              id="show_Note"></textarea>
                                    <input type="hidden" name="Note_original" id="show_Note_info" value="">
                                </div>
                                <div class="form-group">
                                    <label for="link" class="control-label">Länkar</label>
                                    <div id="show_links"></div>
                                </div>
                                <div class="form-group author" style="display: none;">
                                    <label for="name" class="control-label">E-mail</label>
                                    <input class="form-control" name="email" type="text" id="email">
                                </div>
                                <div class="form-group author" style="display: none;">
                                    <label for="name" class="control-label">Telefon</label>
                                    <input class="form-control" name="phone" type="text" id="phone">
                                </div>
                                <div class="form-group author" style="display: none;">
                                    <label for="name" class="control-label">Adress</label>
                                    <input class="form-control" name="streetAddress" type="text" id="streetAddress">
                                </div>
                                <div class="form-group author" style="display: none;">
                                    <label for="name" class="control-label">Postnummer</label>
                                    <input class="form-control" name="postalCode" type="text" id="postalCode">
                                </div>
                                <div class="form-group author" style="display: none;">
                                    <label for="name" class="control-label">Land</label>
                                    <input class="form-control" name="country" type="text" id="country">
                                </div>
                                <div class="form-group geo" id="geo">
                                    <label for="name" class="control-label">GEO: latiude and longitude</label>
                                    <input class="form-control" name="geo" type="text" id="geo">

                                </div>
                                </fieldset>
                            </td>
                            <td>
                                <div class="form-group">
                                    <label class="control-label">Dina ändringförslag</label>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">Namn</label>
                                    <input class="form-control" name="name" type="text" id="proposal_name">
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">Lång beskrivning</label>
                                    <textarea class="form-control" rows="4" name="description_long"
                                              id="proposal_description_long"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">Kort beskrivning</label>
                                    <input class="form-control" name="description_short" type="text"
                                           id="proposal_description_short">
                                </div>
                                <div class="form-group">
                                    <label for="note" class="control-label">Notering</label>
                                    <textarea class="form-control" rows="2" name="Note" id="proposal_Note"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">Typ</label>
                                    <select class="form-control" name="type" id="proposal_tagtype" disabled>
                                        <option value="">Välj en typ</option>
                                        <option value="person">Person</option>
                                        <option value="organisation">Organisation</option>
                                        <option value="event">Evenemang</option>
                                        <option value="object">Övrigt</option>
                                        <option value="story">Story</option>
                                        <option value="category">category</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="add_links_form">


                                    </div>
                                    <label for="addedlinks" class="control-label addedlinks" style="display: none;">Lista
                                        på länkar</label>
                                    <div class="input_fields_wrap">
                                        <div class="tag-row">
                                            <div class="form-inline">
                                                <input type="text" class="form-control" name="links"/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Stäng</button>
                    <button type="submit" class="submitviewbutton btn btn-primary">Spara</button>
                    <span><img src="assets/images/ajax-loader.gif" alt="Laddar" id="loginiconform"></span>
                </div>
            </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->


