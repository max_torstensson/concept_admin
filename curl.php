<?php
    /**
     * Get CURL value for XML file
     * @param $filename
     * @return string
     */
    function getCurlValue($filename)
    {
        // PHP 5.6
        $value = new CURLFile($filename); // Support for PHP 5.6

        // Use the old style if using an older version of PHP
        //$value = "@" .realpath($filename);
        return $value;
    }