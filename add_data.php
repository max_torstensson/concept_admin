<?php
session_start();
include 'functions.php';
$target_dir = "assets/upload/";
date_default_timezone_set("Europe/Stockholm");
$current_date = new DateTime(date("Y-m-d H:i:s"));
$date_created = $current_date->format("Y-m-d") . "T" . $current_date->format("H:i:s") . "Z";
$date_versionCreated = $current_date->format("Y-m-d") . "T" . $current_date->format("H:i:s") . "Z";
$guid = getGUID();
$status = "withheld";



$str = $_POST['data'];

parse_str($str, $arr);

/**
 * Values from form
 */
$name = $arr['name'];
$type = $arr['type'];

$description_long = $arr['description_long'];
$description_short = $arr['description_short'];
$links = $_POST['formData'];
$note = $arr['Note'];


/** author */
$email = $arr['email'];
$phone = $arr['phone'];
$streetadress = $arr['streetAddress'];
$postalcode = $arr['postalCode'];
$country = $arr['country'];
$note = "";


if ($type == "person" || $type == "author") {

    $firstname = trim($arr['firstname']);
    $lastname = trim($arr['lastname']);
    $name = trim($firstname . " ". $lastname);
}
/**
 * If type is empty, not add a tags
 */
if ($type == "") {
    die();
}

/**
 * Create XML file
 *
 */

$xml_filename = create_xml_file($guid, $date_versionCreated, $date_created, $name,$firstname,$lastname, $email,$phone, $streetadress, $postalcode, $country, $type, $description_long, $description_short, $target_dir, $status, $note, $links, $np,$ad);
$xmlFile = getCurlValue($target_dir . $xml_filename);

/**
 * Upload to OC
 */
upload_to_oc($xml_filename, $xmlFile, $guid);

/**
 * Remove temporary file
 */
if (!unlink($target_dir . $xml_filename)) {
    $message .= " Det gick inte att ta bort temporärfilen för xml";
    $success = false;
}