<?php
session_start();
include 'functions.php';

if (!isset($_GET['action'])) {
    /**
     * Get data from search results
     */
    $editvalid = true;
    $totalhits = 0;
    $data = getTags(true);
    $data = $data->hits;
    $totalhits = $data->totalHits;
    $hits = $data->hits;
    $returnvalue = "";
    foreach ($hits as $tag):
        $returnvalue .= "<tr class=\"datarow v \" data-tagid=\"" . $tag->id . "\" id=\"datarow_" . $tag->id . "\">";
        if (GetValidEdit($tag->versions[0]->properties->ConceptImType[0])) {
            $returnvalue .= "<td><button title=\"Redigera taggen\" data-id=\"" . $tag->id . "\" data-tagname=\"" . $tag->versions[0]->properties->ConceptName[0] . "\" class=\"btn btn-sm btn-primary btnEdit hvr-grow hvr-glow \" role=\"button\"><i class=\"fa fa-pencil\"></i></button></td>";
        } else {
            $returnvalue .= "<td><button disabled title=\"Concept-admin stödjer inte denna typ att redigera.\" data-id=\"" . $tag->id . "\" data-tagname=\"" . $tag->versions[0]->properties->ConceptName[0] . "\" class=\"btn btn-sm btn-danger btnEdit\" role=\"button\"><i class=\"fa fa-pencil\"></i></button></td>";
        }
        $returnvalue .= "<td><a id=\"status_" . $tag->id . "\" class=\"hvr-glow btn btn-status btn-sm  " . get_tagstatus_class($tag->versions[0]->properties->ConceptStatus[0]) . " btnStatus\" data-links=\"\" data-id=\"" . $tag->id . "\" data-currentstatus=\"" . $tag->versions[0]->properties->ConceptStatus[0] . "\" role=\"button\">" . get_tagstatus_text($tag->versions[0]->properties->ConceptStatus[0]) . "</a></td>";
        $returnvalue .= "<td>" . $tag->versions[0]->properties->ConceptName[0] . "</td>";
        $returnvalue .= "<td>" . $tag->versions[0]->properties->ConceptImType[0] . "</td>";
        $returnvalue .= "<td>" . convert_date_from_oc($tag->versions[0]->properties->VersionCreated[0]) . "</td>";
        $returnvalue .= "<td>" . hasProperty("ConceptDefinitionLong", $tag->versions[0]->properties, "fa-sticky-note-o", "Lång beskrivning") . "</td>";
        $returnvalue .= "<td>" . hasProperty("ConceptDefinitionShort", $tag->versions[0]->properties, "fa-file-o", "Kort beskrivning") . "</td>";
        $returnvalue .= "<td>" . hasProperty("ConceptSeeAlso", $tag->versions[0]->properties, "fa-link", "Länkar") . "</td>";
        $returnvalue .= "</tr>";
    endforeach;

    print_r(json_encode(array(
        $totalhits,
        $returnvalue
    )));
} else if ($_GET['action'] == "gettag_by_name") {
    /**
     * Get data from one tag
     */
    $data = getTags(false);
    $data = $data->hits;
    $hits = $data->hits;
    foreach ($hits as $tag):
        print_r(json_encode($tag));
    endforeach;
} else {
    /**
     * Get data from one tag
     */
    $guid = $_GET['id'];
    $data = getOneTag($guid);
    print_r($data);
}
function GetValidEdit($type)
{
    switch ($type) {
        case "person":
            return true;
        case "author":
            return true;
        case "category";
            return true;
        case "content-profile";
            return true;
        case "organisation";
            return true;
        case "story";
            return true;
        case "place";
            return true;
        case "topic":
            return true;
        default:
            return false;
    }
}

