<?php
session_start();
include 'functions.php';
$target_dir = "assets/upload/";
$guid = $_GET['id'];
$message = "";

date_default_timezone_set("Europe/Stockholm");
$current_date = new DateTime(date("Y-m-d H:i:s"));
$date_versionCreated = $current_date->format("Y-m-d") . "T" . $current_date->format("H:i:s") . "Z";

/**
 * Update only status for a tag
 */
if ($_GET['action'] == "updatestatus") {
    /**
     * Information from form
     */

    $status = $_GET['current'];
    $status = get_next_tagstatus($status);
    if ($guid != "") {
        /**
         * Get information from OC with object $_GET['id']
         */
        $data = getOneTag($guid);
        $properties = json_decode($data)->properties;

        // Get values from properties on object
        $name = get_value_in_oc_properties("ConceptName", $properties)[0];
        // $type = get_value_in_oc_properties("ConceptType", $properties)[0];
        $description_long = get_value_in_oc_properties("ConceptDefinitionLong", $properties)[0];
        $description_short = get_value_in_oc_properties("ConceptDefinitionShort", $properties)[0];
        $date_created = get_value_in_oc_properties("created", $properties)[0];
        $link_array = get_value_in_oc_properties("ConceptSeeAlso", $properties);
        $links = "";
        $email = "";
        $phone = "";
        $postalcode = "";
        $streetadress = "";
        $country = "";
        $note = "";


        foreach ($link_array as $link) {
            $links .= $link . ",";
        }
        $links = rtrim($links, ",");

        /**
         * Create XML file
         */
        $ex_xml_file = getOneTagXMl($guid);
        var_dump($ex_xml_file);
        $xml_filename = Update_status($guid, $target_dir, $status, $ex_xml_file,$date_versionCreated);
        $xmlFile = getCurlValue($target_dir . $xml_filename);
        /**
         * Upload to OC
         *
         */

        $metadata = get_value_in_oc_properties("metadata", $properties)[0];
        var_dump($metadata);
        upload_to_oc($xml_filename, $xmlFile, $guid);
        /**
         * Remove temporary file
         */
        if (!unlink($target_dir . $xml_filename))
            $message .= " Det gick inte att ta bort temporärfilen för xml";
    }
}

/**
 * Update complete tag information
 */
if ($_GET['action'] == "updatetag") {
    $str = $_POST['data'];
    parse_str($str, $arr);
    /**
     * Values from form
     */

    $data = getOneTag($guid);
    $properties = json_decode($data)->properties;

    $name = $arr['name'];
    $type = $arr['type'];
    $description_long = $arr['description_long'];
    $description_short = $arr['description_short'];
    $date_created = $arr['date_created'];

    //links
    $links = $_POST['formData'];
    $removedlinks = $_POST['removedlinks'];

    ///
    $status = get_value_in_oc_properties("ConceptStatus", $properties)[0];
    $link_array_oc = get_value_in_oc_properties("ConceptSeeAlso", $properties);

    /** author */
    $email = $arr['email'];
    $phone = $arr['phone'];
    $streetadress = $arr['streetAddress'];
    $postalcode = $arr['postalCode'];
    $country = $arr['country'];

    /** person */
    if ($type == "person" || $type == "author") {
        $firstname = trim($arr['firstname']);
        $lastname = trim($arr['lastname']);
        $name = trim($firstname . " ". $lastname);
    }

    /**
     * If name or type is empty, not add a tags
     */
    if ($type == "") {
        die();
    }

    /**
     * Create XML file
     */
    $ex_xml_file = getOneTagXMl($guid);
    $xml_filename_file = update_xml_file($guid, $date_versionCreated, $name, $firstname, $lastname, $email, $phone, $streetadress, $postalcode, $country, $type, $description_long, $description_short, $target_dir, $status, $links,$removedlinks, $ex_xml_file, $link_array_oc);
    $xmlFile_file = getCurlValue($target_dir . $xml_filename_file);
    /**
     * Upload proposal and file file to OC
     */
    upload_to_oc($xml_filename_file, $xmlFile_file, $guid);

    /**
     * Remove temporary xml file
     */
    if (!unlink($target_dir . $xml_filename_file))
        $message .= " Det gick inte att ta bort temporärfilen för xml";

    /**
     * Remove temporary proposal file
     */
    if (!unlink($target_dir . $xml_filename_proposal))
        $message .= " Det gick inte att ta bort temporärfilen för proposalxml-filen";
    }

/**
 * Delete tag
 */
if ($_GET['action'] == "deletetag") {
    $url = get_oc_api_base_path() . "/objects/" . $guid;
    $data = get_api_data($url, get_oc_api_credentials(), "DELETE");
}

/**
 * Update proposals for a concept
 */

/**
 * Update userData
 */
if ($_GET['action'] == "updateUserData") {
    // Get form data
    $articleid = $_GET['id'];
    $etag = $_POST['etag'];
    $userdata = $_POST['userdata'];
    $tags = $_POST['tags'];
    $stories = $_POST['stories'];

    // Make a new call and get etag
    // URL to get article user data
    $url = get_np_api_base_path() . "/articles/" . $articleid . "/userData";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('charset=UTF-8'));
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_USERPWD, get_np_api_credentials());
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $data = curl_exec($ch);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($data, 0, $header_size);
    $body = substr($data, $header_size);
    curl_close($ch);

    $headers = explode("\n", $header);
    $etag = get_etag($headers);

    // Check if we have conceptTags. Replace content in <conceptTags> with value in $tags
    if (!strpos($userdata, "conceptTags"))
        $userdata = str_replace("<userdata>", "<userdata><conceptTags valueId=\"\">" . $tags . "</conceptTags>", $userdata);
    else
        $userdata = replace_between($userdata, "<conceptTags valueId=\"\">", "</conceptTags>", $tags);

    // Check if we have conceptStories. Replace content in <conceptStories> with value in $stories
    if (!strpos($userdata, "conceptStories"))
        $userdata = str_replace("<userdata>", "<userdata><conceptStories valueId=\"\">" . $stories . "</conceptStories>", $userdata);
    else
        $userdata = replace_between($userdata, "<conceptStories valueId=\"\">", "</conceptStories>", $stories);

    // URL for update user data
    $url = get_np_api_base_path() . "/articles/" . $articleid . "/userData";

    // Header value etag from GET command
    $header = array(
        "If-Match: " . "\"" . $etag . "\"",
        "Content-Type: application/xml"
    );

    // CURL call
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $userdata);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_USERPWD, get_np_api_credentials());
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $data = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
}
