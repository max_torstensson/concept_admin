<?php
session_start();
include 'functions.php';

if($_GET['function'] == "tag") {
    $data = getTags(true, "tag");
    $data = $data->hits;
    $hits = $data->hits;
    $returnvalue = "";
    if (sizeof($hits) > 0) {
        $returnvalue .= "<table class=\"table newspilot-result\">";
        foreach ($hits as $tag):
            $returnvalue .= "<tr data-tagid=\"" . $tag->id . "\" id=\"datarow_" . $tag->id . "\">";
            $returnvalue .= "<td><button class=\"btn " .get_tagstatus_class($tag->versions[0]->properties->ConceptStatus[0]) ." btn-circle btn-xs\" type=\"button\"> </button> " . $tag->versions[0]->properties->ConceptName[0] . " <span class=\"tag_short_descr\">" . $tag->versions[0]->properties->ConceptDefinitionShort[0] . "</span></td>";

            $returnvalue .= "<td><a title=\"Visa/Ändra\" data-tagid=\"" . $tag->id . "\" class=\"btn btn-sm btn-default showedit\" role=\"button\">Visa/Ändra</a></td>";

            if($tag->versions[0]->properties->ConceptStatus[0] != "cancelled")
                $returnvalue .= "<td><a title=\"Välj taggen\" data-tagstatus=\"" . $tag->versions[0]->properties->ConceptStatus[0] . "\" data-tagid=\"" . $tag->id . "\" data-tagname=\"" . $tag->versions[0]->properties->ConceptName[0] . "\" class=\"btn btn-sm btn-primary addtocollection\" role=\"button\">Välj tagg</a></td>";
            else
                $returnvalue .= "<td></td>";
            $returnvalue .= "</tr>";
        endforeach;
        $returnvalue .= "</table>";
    }
    print_r(json_encode(
        $returnvalue
    ));
}
if($_GET['function'] == "story") {
    $data = getTags(true, "story");
    $data = $data->hits;
    $hits = $data->hits;
    $returnvalue = "";
    if (sizeof($hits) > 0) {
        $returnvalue .= "<table class=\"table newspilot-story-result\">";
        foreach ($hits as $tag):
            $returnvalue .= "<tr data-tagid=\"" . $tag->id . "\" id=\"story_datarow_" . $tag->id . "\">";
            $returnvalue .= "<td><button class=\"btn " .get_tagstatus_class($tag->versions[0]->properties->ConceptStatus[0]) ." btn-circle btn-xs\" type=\"button\"> </button> " . $tag->versions[0]->properties->ConceptName[0] . " <span class=\"tag_short_descr\">" . $tag->versions[0]->properties->ConceptDefinitionShort[0] . "</span></td>";

            $returnvalue .= "<td><a title=\"Visa/Ändra\" data-tagid=\"" . $tag->id . "\" class=\"btn btn-sm btn-default showedit\" role=\"button\">Visa/Ändra</a></td>";

            if($tag->versions[0]->properties->ConceptStatus[0] != "cancelled")
                $returnvalue .= "<td><a title=\"Välj storyn\" data-tagstatus=\"" . $tag->versions[0]->properties->ConceptStatus[0] . "\" data-tagid=\"" . $tag->id . "\" data-tagname=\"" . $tag->versions[0]->properties->ConceptName[0] . "\" class=\"btn btn-sm btn-primary addtocollection_story\" role=\"button\">Välj story</a></td>";
            else
                $returnvalue .= "<td></td>";
            $returnvalue .= "</tr>";
        endforeach;
        $returnvalue .= "</table>";
    }
    print_r(json_encode(
      $returnvalue
    ));
}
elseif($_GET['function'] == "articletags") {
    $nparticleId = $_GET['articleid'];

    // URL to get article user data
    $url = get_np_api_base_path() . "/articles/" .$nparticleId ."/userData";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('charset=UTF-8'));
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_USERPWD, get_np_api_credentials());
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $data = curl_exec( $ch );
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($data, 0, $header_size);
    $body = substr($data, $header_size);
    curl_close($ch);

    $headers = explode("\n", $header);
    $etag = get_etag($headers);
    $userdata = str_replace(array("\r", "\n"), '', $body);

    // Get all data from conceptTags
    $tags_query = "";
    $tags_uuid = getTextBetweenTags($userdata, "conceptTags")[1];

    // Get all data from conceptStories
    $stories_query = "";
    $stories_uuid = getTextBetweenTags($userdata, "conceptStories")[1];

    // Search for tags in OC
    if($tags_uuid != "" && sizeof($tags_uuid) > 0)
        $tags_query = "uuid:" .str_replace(",", " OR uuid:", $tags_uuid[0]);
    $data = searchForTags($tags_query);
    if(isset($data->hits->hits))
        $tag_hits = $data->hits->hits;
    else
        $tag_hits = null;

    // Search for stories in OC
    if($stories_uuid != "" && sizeof($stories_uuid) > 0)
        $stories_query = "uuid:" .str_replace(",", " OR uuid:", $stories_uuid[0]);
    $data = searchForTags($stories_query);
    if(isset($data->hits->hits))
        $stories_hits = $data->hits->hits;
    else
        $stories_hits = null;

    print_r(json_encode(array(
      $tag_hits, $userdata, $etag, $nparticleId, $stories_hits
    )));
}