<?php
session_start();
include 'functions.php';
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Concept admin | MittMedia</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="assets/main.css" rel="stylesheet">
    <script src="http://www.myersdaily.org/joseph/javascript/md5.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places,geometry&amp;sensor=false&amp;language=fr" ></script>
    <link href="css/hover.css" rel="stylesheet" media="all">
</head>
<body>
<?php include 'modal.php'; ?>
<div id="wrapper">
    <?php include 'menu.php'; ?>
    <div id="page-wrapper" class="white-bg ">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg " role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" method="post" action="#">
                        <div class="form-group">
                            <input type="text" class="form-control inline" id="search" name="search" autocomplete="off" title="Sök efter en tagg (minst två tecken)" placeholder="Klicka här för att söka...">
                            <span><img src="assets/images/ajax-loader.gif" alt="Laddar" id="loadingicon"></span>
                        </div>
                    </form>
                </div>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2 id="tagTitle">Senaste skapade taggar</h2>
            </div>
            <div class="col-lg-2">
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12" style="background: #fff;">
                    <div id="message"></div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-5 m-b-xs">
                                    <select class="input-sm form-control input-s-sm inline" name="search_sorting" id="search_sorting">
                                        <!--<option value="relevans">Sortering: relevans</option> -->
                                        <option value="Publiceringsdag">Sortering: datum. Skapad först.</option>
                                        <option value="updated" selected="selected">Sortering: datum. Uppdaterad först.</option>
                                    </select>
                                </div>
                                <div class="col-sm-4 m-b-xs">
                                    <div class="btn-group" role="group" aria-label="">
                                        <button type="button" class="btn btn-white active status_button hvr-shrink" data-status="withheld" id="status_withheld" data-toggle="button" aria-pressed="true" autocomplete="off">Väntar</button>
                                        <button type="button" class="btn btn-white active status_button hvr-shrink" data-status="usable" id="status_usable" data-toggle="button" aria-pressed="true" autocomplete="off">Godkänd</button>
                                        <button type="button" class="btn btn-white active status_button hvr-shrink" data-status="cancelled" id="status_cancelled" data-toggle="button" aria-pressed="true" autocomplete="off">Spärrad</button>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <select class="input-sm form-control input-s-sm inline" name="search_type" id="search_type">
                                        <option value="all">Alla</option>
                                        <option value="author"><i class="fa fa-user"></i> Author</option>
                                        <option value="content-profile">Funktionstagg</option>
                                        <option value="category">Kategori</option>
                                        <option value="organisation">Organisation</option>
                                        <option value="place">Plats</option>
                                        <option value="person">Person</option>
                                        <option value="story">Story</option>
                                        <option value="topic">Ämne</option>
                                    </select>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-condensed" id="tagTable">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Status</th>
                                        <th>Namn</th>
                                        <th>Typ</th>
                                        <th>Uppdaterad</th>
                                        <th><i class="fa fa-sticky-note-o" title="Lång beskrivning"></i></th>
                                        <th><i class="fa fa-file-o" title="Kort beskrivning"></i></th>
                                        <th><i class="fa fa-link" title="Länkar"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer fixed">
            <div>
                <div id="paging"></div>
            </div>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/jtable table-condenseds?key=AIzaSyC_S8mUPLHKN-UhU5eJaUZ1dsvWT-mrRrQ"></script>
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/main.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>


</body>
</html>

