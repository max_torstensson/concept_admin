<?php
session_start();
include 'functions.php';

/**
 * Denna sida används i newspilot för att administrera taggar för användare
 *
 */
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Taggar</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="assets/main.min.css?time=<?php print time() ?>" rel="stylesheet">
</head>
<body class="top-navigation">
<?php include 'modal.php'; ?>
<div id="wrapper newspilotembed">
    <div id="page-wrapper" class="gray-bg">
        <div id="debug"></div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-xs-7">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">s
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Taggar</a></li>
                            <li role="presentation"><a href="#story" aria-controls="profile" role="tab" data-toggle="tab">Stories</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="nptagsearch">
                                    <b>Skriv namnet på taggen här:</b>
                                    <input type="text" class="form-control inline" id="search_newspilot" name="search_newspilot" autocomplete="off" title="Sök efter en tagg (minst två tecken)" placeholder="">
                                    <div id="search_in_progress">Sökning pågår...</div>
                                </div>
                                <div id="search_newspilot_result"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="story">
                                <div class="nptagsearch">
                                    <b>Börja skriva namnet på en story:</b>
                                    <input type="text" class="form-control inline" id="search_newspilot_story" name="search_newspilot_story" autocomplete="off" title="Sök efter en story (minst två tecken)" placeholder="">
                                    <div id="search_in_progress_story">Sökning pågår...</div>
                                </div>
                                <div id="search_newspilot_result_stories"></div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-5">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Taggar till denna artikel</h5>
                        </div>
                        <div class="ibox-content">
                            <p id="notags"></p>
                            <table class="table" id="newspilot_collection"></table>
                        </div>
                    </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Stories till denna artikel</h5>
                        </div>
                        <div class="ibox-content">
                            <p id="nostories"></p>
                            <table class="table" id="newspilot_stories_collection"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $newspilot = true;
</script>

<!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="assets/main-min.js?time=<?php print time() ?>"></script>

</body>
</html>

