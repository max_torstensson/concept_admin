<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">CONCEPT
                                        ADMIN</strong></span>
                </div>
                <div class="logo-element">
                    CA
                </div>
            </li>
            <li class="active">
                <a class="hvr-underline-from-right" href="<?php $_SERVER["DOCUMENT_URI"] ?>"><i class="fa fa-th-large"></i> <span
                        class="nav-label">Översikt</span></a>
            </li>
            <li>
                <a class="hvr-underline-from-right" id="verifymode"><i class="fa fa-check"></i> <span class="nav-label">Verifiera taggar</span></a>
            </li>
            <li>
                <a class="hvr-underline-from-right" id="createtag"><i class="fa fa-pencil"></i> <span class="nav-label">Skapa tagg</span></a>
            </li>
        </ul>
    </div>
</nav>